import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UmeshPagePage } from './umesh-page.page';

const routes: Routes = [
  {
    path: '',
    component: UmeshPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UmeshPagePageRoutingModule {}
