import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgCalendarModule } from "ionic2-calendar";
import { CalenderComponentComponent } from  "../calender-component/calender-component.component"


@NgModule({
  declarations: [CalenderComponentComponent],
  imports: [
  CommonModule,
  FormsModule,
  NgCalendarModule,
  IonicModule,
  ],
  exports: [CalenderComponentComponent]
})
export class CalenderComponentModule { }
