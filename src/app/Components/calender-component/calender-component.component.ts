import {
  Component,
  OnInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { CalendarComponent } from "ionic2-calendar";

@Component({
  selector: "app-calender-component",
  templateUrl: "./calender-component.component.html",
  styleUrls: ["./calender-component.component.scss"],
})
export class CalenderComponentComponent implements OnInit {
  @Input() eventSource = [];
  @Output() onEventClicked: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  viewTitle: string;
  calendar = {
    mode: "month",
    currentDate: new Date(),
  };
  constructor() {}

  ngOnInit() {
    // this.myCal.loadEvents();
  }

  //  Change current month/week/day
  next() {
    this.myCal.slideNext();
  }

  back() {
    this.myCal.slidePrev();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  onEventSelected(event) {
    // console.log(event)
    this.onEventClicked.emit(event);
  }
}
