import { Component, OnInit, Input } from "@angular/core";
import { Notifications } from "src/app/Models/notificationInfo";
import { NotificationServiceService } from "src/app/Services/notification-service.service";
import { Router } from "@angular/router";
import { PopoverController } from "@ionic/angular";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ToastServiceService } from "src/app/Services/toast-service.service";

@Component({
  selector: "app-test-details-component",
  templateUrl: "./test-details-component.component.html",
  styleUrls: ["./test-details-component.component.scss"],
})
export class TestDetailsComponentComponent implements OnInit {
  @Input() notification: Notifications;
  showEndDate: boolean = false;
  showEndTime: boolean = false;
  constructor(
    private router: Router,
    private popoverController: PopoverController,
    private notificationService: NotificationServiceService,
    private toastService: ToastServiceService,
    private storageService: StorageServiceService
  ) {}

  ngOnInit() {
    console.log("in component", this.notification);
    this.checkEndDate(this.notification.EndDate, this.notification.EndTime);
  }

  checkEndDate(testEndDate, endTime) {
    var endDateSplit = testEndDate.split("-");
    var endTimeSplit = endTime.split(":");

    var dayEndDate = endDateSplit[0];
    var monthEndDate = endDateSplit[1];
    var yearEndDate = endDateSplit[2];

    var hourEndTime = endTimeSplit[0];
    var minutEndTime = endTimeSplit[1];
    var endMinutSplit = minutEndTime.split(" ");
    var finalEndMinut = endMinutSplit[0];
    var secEndTime = "00";

    var finalEndDate =
      yearEndDate +
      "/" +
      monthEndDate +
      "/" +
      dayEndDate +
      " " +
      hourEndTime +
      ":" +
      finalEndMinut +
      ":" +
      secEndTime;

    var testEndDateTime = Math.round(new Date(finalEndDate).getTime() / 1000);

    var currentDateTime = Math.round(+new Date().getTime() / 1000);

    var checkDate = Math.round(
      new Date("2020/11/09 07:18:00").getTime() / 1000
    );

    if (checkDate == testEndDateTime) {
      this.showEndDate = false;
      this.showEndTime = false;
    } else {
      this.showEndDate = true;
      this.showEndTime = true;
    }
  }

  startTest() {
    var data = this.notification.TestObj;

    var testStartDate = this.notification.TestDate;
    var testEndDate = this.notification.EndDate;
    var startTime = this.notification.StartTime;
    var endTime = this.notification.EndTime;

    var startDateSplit = testStartDate.split("-");
    var startTimeSplit = startTime.split(":");

    var dayStartDate = startDateSplit[0];
    var monthStartDate = startDateSplit[1];
    var yearStartDate = startDateSplit[2];

    var hourStartTime = startTimeSplit[0];
    var minutStartTime = startTimeSplit[1];
    var startMinutSplit = minutStartTime.split(" ");
    var finalStartMinut = startMinutSplit[0];
    var startAMPM = startMinutSplit[1];
    var secStartTime = "00";

    var endDateSplit = testEndDate.split("-");
    var endTimeSplit = endTime.split(":");

    var dayEndDate = endDateSplit[0].split(" ").join("");
    var monthEndDate = endDateSplit[1].split(" ").join("");
    var yearEndDate = endDateSplit[2].split(" ").join("");

    var hourEndTime = endTimeSplit[0].split(" ").join("");
    var minutEndTime = endTimeSplit[1];
    var endMinutSplit = minutEndTime.split(" ");
    var finalEndMinut = endMinutSplit[0].split(" ").join("");
    var endAMPM = endMinutSplit[1].split(" ").join("");
    var secEndTime = "00";

    var finalStartDate =
      yearStartDate +
      "/" +
      monthStartDate +
      "/" +
      dayStartDate +
      " " +
      this.convertTime12to24(
        hourStartTime + ":" + finalStartMinut + " " + startAMPM
      );

    var finalEndDate =
      yearEndDate +
      "/" +
      monthEndDate +
      "/" +
      dayEndDate +
      " " +
      this.convertTime12to24(hourEndTime + ":" + finalEndMinut + " " + endAMPM);

    var finalEndDate1 =
      yearEndDate +
      "/" +
      monthEndDate +
      "/" +
      dayEndDate +
      " " +
      hourEndTime +
      ":" +
      finalEndMinut +
      ":" +
      "00";

    var testStartDateTime = new Date(finalStartDate).getTime();
    var testEndDateTime = new Date(finalEndDate).getTime();
    var testEndDateTime1 = new Date(finalEndDate1).getTime();

    // console.log(new Date(finalStartDate));
    // console.log(new Date(finalEndDate));

    var curDays = new Date().getDate();
    var curMonths = new Date().getMonth() + 1;
    var curYesrs = new Date().getFullYear();
    var curHourss = new Date().getHours();
    var curMini = new Date().getMinutes();
    var curSecc = new Date().getSeconds();

    var finalCurr =
      curYesrs +
      "/" +
      curMonths +
      "/" +
      curDays +
      " " +
      curHourss +
      ":" +
      curMini +
      ":" +
      curSecc;

    var currentDateTime = new Date(finalCurr).getTime();
    // console.log(new Date(finalCurr));

    var checkDate = new Date("2020/11/09 07:18:00").getTime();
    var ID = this.notification.ID;
    if (checkDate == testEndDateTime1) {
      // console.log(data);
      var da = JSON.parse(data);
      // console.log(data);

      // console.log(da.CurrentDateTime);

      var examDate: any = this.notification.TestDate;

      var ExamDateFormat =
        examDate.split("-")[2] +
        "-" +
        examDate.split("-")[1] +
        "-" +
        examDate.split("-")[0];

      var result = da.CurrentDateTime;

      var todayDateTime = new Date();

      var serverDate = result.split("T")[0];

      var serverTime = result.split("T")[1].substring(0, 5);

      var month: any = todayDateTime.getMonth() + 1;

      if (month.toString().length == 1) month = "0" + month;

      var tDate = todayDateTime.getDate().toString();

      if (tDate.length == 1) tDate = "0" + tDate;

      var todayDate = todayDateTime.getFullYear() + "-" + month + "-" + tDate;

      if (todayDate == serverDate) {
        try {
          var todayDateFormat = new Date(
            todayDateTime.getFullYear(),
            parseInt(month) - 1,
            todayDateTime.getDate()
          );
          var examDateFormatDate: any = new Date(
            examDate.split("-")[2],
            parseInt(examDate.split("-")[1]) - 1,
            examDate.split("-")[0]
          );
        } catch (err) {}
        if (ExamDateFormat == todayDate) {
          var hours = todayDateTime.getHours();
          var minutes: any = todayDateTime.getMinutes();
          var ampm = hours >= 12 ? "PM" : "AM";
          hours = hours % 12;
          hours = hours ? hours : 12;
          minutes = minutes < 10 ? "0" + minutes : minutes;
          var strTime = hours + ":" + minutes + " " + ampm;

          var monthNames = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
          ];

          var datedetails =
            monthNames[todayDateTime.getMonth()] +
            " " +
            todayDateTime.getDate() +
            ", " +
            todayDateTime.getFullYear() +
            " ";
          var stt: any = new Date(datedetails + strTime);

          stt = stt.getTime();
          var endt: any = new Date(datedetails + this.notification.StartTime);

          endt = endt.getTime();

          var beforefiveminutes = endt;
          var afterfiveminutes = endt + 1800000;

          var diff = (endt - stt) / 1000 / 60 / 60;

          if (stt >= beforefiveminutes && stt <= afterfiveminutes) {
            this.notificationService.startTest(ID, "read").then((_) => {
              this.storageService.testExpired = "false";
              this.popoverController.dismiss().then((_) => {
                this.storageService.notification = this.notification;
                this.storageService.page = "unit-test";
                this.router.navigateByUrl("/online-test-page");
              });
            });
          } else if (stt < beforefiveminutes) {
            this.toastService.createToast("You are early for the test.", 3000);
          } else if (stt > afterfiveminutes) {
            this.notificationService.startTest(ID, "expired").then((_) => {
              this.storageService.testExpired = "true";
              this.popoverController.dismiss().then((_) => {
                this.toastService.createToast(
                  "Sorry your test time is out.",
                  3000
                );
              });
            });
          }
        } else if (examDateFormatDate > todayDateFormat) {
          this.toastService.createToast("You are early for the test.", 3000);
        } else if (examDateFormatDate < todayDateFormat) {
          this.notificationService.startTest(ID, "expired").then((_) => {
            this.storageService.testExpired = "true";
            this.popoverController.dismiss().then((_) => {
              this.toastService.createToast(
                "Sorry your test time is out.",
                3000
              );
            });
          });
        }
      } else {
        this.toastService.createToast("Device date is incorrect.", 3000);
      }
    } else {
      if (currentDateTime < testStartDateTime) {
        this.toastService.createToast("please Start Test On Time", 3000);
      } else if (
        currentDateTime <= testEndDateTime &&
        currentDateTime >= testStartDateTime
      ) {
        this.notificationService.startTest(ID, "read").then((_) => {
          this.storageService.testExpired = "false";
          this.popoverController.dismiss().then((_) => {
            this.storageService.notification = this.notification;
            this.storageService.page = "unit-test";
            this.router.navigateByUrl("/online-test-page");
          });
        });
      } else if (currentDateTime > testEndDateTime) {
        this.notificationService.startTest(ID, "expired").then((_) => {
          this.storageService.testExpired = "true";
          this.popoverController.dismiss().then((_) => {
            this.toastService.createToast("Sorry your test time is out.", 3000);
          });
        });
      }
    }
  }

  convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(" ");

    let [hours, minutes] = time.split(":");

    if (hours === "12") {
      hours = "00";
    }

    if (modifier === "PM") {
      hours = parseInt(hours, 10) + 12;
    }

    return `${hours}:${minutes}`;
  }
}
