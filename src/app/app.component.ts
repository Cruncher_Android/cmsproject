import { Component } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Platform, AlertController, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { DatabaseService } from "./Services/database.service";
import { Router } from "@angular/router";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { StorageServiceService } from "./Services/storage-service.service";
import { UserDetailsService } from "src/app/Services/user-details.service";
import { ApiServiceService } from "./Services/api-service.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private fcm: FCM,
    private navController: NavController,
    private alertController: AlertController,
    private databaseService: DatabaseService,
    private storageService: StorageServiceService,
    private userDetailsService: UserDetailsService,
    private apiService: ApiServiceService
  ) {
    this.initializeApp();

    this.platform.backButton.subscribeWithPriority(0, () => {
      if (router.url == "/login-page") {
        this.createAlert();
      } else if (router.url == "/dashboard-page/home-page") this.createAlert();
      else if (
        router.url == "/forgot-password-page" &&
        storageService.forgotPassword
      ) {
        // console.log("in else")
        navController.back();
      } else if (router.url.startsWith("/dashboard-page/"))
        router.navigateByUrl("/dashboard-page/home-page");
      else {
        navController.back();
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.databaseService.createDataBase();
      this.storage.get("userLoggedIn").then((data) => {
        if (data) {
          this.router.navigateByUrl("/dashboard-page/home-page");
          this.statusBar.backgroundColorByHexString("#0d4fc3");
          this.fcm.onTokenRefresh().subscribe((token) => {
            console.log(token);
            this.apiService
              .setToken(this.storageService.userDetails.userId.trim(), token)
              .then((data) => {
                console.log(data.data);
                this.storageService.tokenId = token;
                this.storage.set("token", token);
              });
          });
        } else {
          this.router.navigateByUrl("/login-page");
          this.statusBar.backgroundColorByHexString("#ffffff");
          this.statusBar.styleDefault();
        }
      });
      this.statusBar.show();
      this.splashScreen.hide();
      this.fcm.onNotification().subscribe((data) => {
        console.log(data);
        if (data.wasTapped) {
          console.log("Received in background");
          // this.router.navigate([data.landing_page, data.price]);
        } else {
          console.log("Received in foreground");
          // this.router.navigate([data.landing_page, data.price]);
        }
      });
    });
  }

  async createAlert() {
    const alert = await this.alertController.create({
      header: "Close App",
      cssClass: "alert-title",
      subHeader: "Do you want to close app ?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Close App",
          handler: () => {
            if (this.router.url != "/home-page")
              this.userDetailsService
                .deleteFromCMSLogStatus()
                .then((_) => navigator["app"].exitApp());
            else navigator["app"].exitApp();
          },
        },
      ],
    });
    await alert.present();
  }
}
