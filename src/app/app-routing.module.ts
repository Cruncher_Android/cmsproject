import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "login-page",
    loadChildren: () =>
      import("./Pages/login-page/login-page.module").then(
        (m) => m.LoginPagePageModule
      ),
  },
  {
    path: "password-page",
    loadChildren: () =>
      import("./Pages/password-page/password-page.module").then(
        (m) => m.PasswordPagePageModule
      ),
  },
  {
    path: "forgot-password-page",
    loadChildren: () =>
      import("./Pages/forgot-password-page/forgot-password-page.module").then(
        (m) => m.ForgotPasswordPagePageModule
      ),
  },
  {
    path: "dashboard-page",
    loadChildren: () =>
      import("./pages/dashboard-page/dashboard-page.module").then(
        (m) => m.DashboardPagePageModule
      ),
  },

  {
    path: "online-test-list-page",
    loadChildren: () =>
      import("./Pages/online-test-list-page/online-test-list-page.module").then(
        (m) => m.OnlineTestListPagePageModule
      ),
  },
  {
    path: "online-test-page",
    loadChildren: () =>
      import("./Pages/online-test-page/online-test-page.module").then(
        (m) => m.OnlineTestPagePageModule
      ),
  },
  {
    path: "online-test-page/:id",
    loadChildren: () =>
      import("./Pages/online-test-page/online-test-page.module").then(
        (m) => m.OnlineTestPagePageModule
      ),
  },
  {
    path: "question-panel",
    loadChildren: () =>
      import("./Components/question-panel/question-panel.module").then(
        (m) => m.QuestionPanelPageModule
      ),
  },

  {
    path: "pdf-list-page/:type",
    loadChildren: () =>
      import("./Pages/pdf-list-page/pdf-list-page.module").then(
        (m) => m.PdfListPagePageModule
      ),
  },

  {
    path: "notice-page",
    loadChildren: () =>
      import("./Pages/notice-page/notice-page.module").then(
        (m) => m.NoticePagePageModule
      ),
  },
  {
    path: "otp-page",
    loadChildren: () =>
      import("./pages/otp-page/otp-page.module").then(
        (m) => m.OtpPagePageModule
      ),
  },
  {
    path: "notification-detail-page/:id",
    loadChildren: () =>
      import(
        "./pages/notification-detail-page/notification-detail-page.module"
      ).then((m) => m.NotificationDetailPagePageModule),
  },
  {
    path: 'demo-page',
    loadChildren: () => import('./demo-page/demo-page.module').then( m => m.DemoPagePageModule)
  },
  {
    path: 'umesh-page',
    loadChildren: () => import('./Umesh/umesh-page/umesh-page.module').then( m => m.UmeshPagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
