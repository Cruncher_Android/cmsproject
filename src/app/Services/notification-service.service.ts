import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { StorageServiceService } from "./storage-service.service";
import {
  LastNotification,
  Notifications,
  Notice,
} from "../Models/notificationInfo";

@Injectable({
  providedIn: "root",
})
export class NotificationServiceService {
  constructor(
    private databaseService: DatabaseService,
    private storageService: StorageServiceService
  ) {}

  getLastNotification() {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSRegistrationDetails LEFT JOIN onlineTestNotifications ON onlineTestNotifications.SId = CMSRegistrationDetails.sId 
        WHERE CMSRegistrationDetails.sId=? ORDER BY ID DESC LIMIT 1`,
        [this.storageService.userDetails.sId]
      )
      .then((data) => {
        // console.log(data);
        let lastNotification: LastNotification;
        if ((data.rows.length = 1)) {
          lastNotification = {
            NBody: data.rows.item(0).NBody,
            NDate: data.rows.item(0).NDate,
            NTime: data.rows.item(0).NTime,
            NOnlineNotificationId: data.rows.item(0).NOnlineNotificationId,
            branchId: data.rows.item(0).branchId,
            classId: data.rows.item(0).classId,
            BatchId: data.rows.item(0).BatchId,
            ROnlineNotificationId: data.rows.item(0).ROnlineNotificationId,
            RPdfUploadId: data.rows.item(0).RPdfUploadId,
            ROnlineTestNotificationId: data.rows.item(0)
              .ROnlineTestNotificationId,
            ArrengeTestId: data.rows.item(0).ArrengeTestId,
            MaxOfflineTestPaperId: data.rows.item(0).MaxOfflineTestPaperId,
            MaxOfflineTestStudentMarksId: data.rows.item(0)
              .MaxOfflineTestStudentMarksId,
            studentName: data.rows.item(0).studentName,
            userId: data.rows.item(0).userId,
            selectedSubjects: data.rows.item(0).selectedSubjects,
          };
          return lastNotification;
        }
      });
  }

  selectFromCMSNotificationMessage() {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSNotificationMessage WHERE Category=? ORDER BY NOnlineNotificationId DESC LIMIT 1`,
        ["Notice"]
      )
      .then((data) => {
        let RNotificationId: string;
        if (data.rows.length == 1)
          RNotificationId = data.rows.item(0).NOnlineNotificationId;
        else RNotificationId = "";
        return RNotificationId;
      });
  }

  getCMSNotificationMessage(sid) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSNotificationMessage WHERE SId=? ORDER BY ID DESC`,
        [sid]
      )
      .then((data) => {
        let noticeDetails: Notice[] = [];
        for (let i = 0; i < data.rows.length; i++) {
          noticeDetails.push({
            Category: data.rows.item(i).Category,
            ID: data.rows.item(i).ID,
            NBody: data.rows.item(i).NBody,
            NDate: data.rows.item(i).NDate,
            NOnlineNotificationId: data.rows.item(i).NOnlineNotificationId,
            NStatus: data.rows.item(i).NStatus,
            NTime: data.rows.item(i).NTime,
            sId: data.rows.item(i).sId,
          });
        }
        return noticeDetails;
      });
  }

  getCMSNotification(notificationId) {
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT * FROM CMSNotificationMessage WHERE ID=? `, [
        notificationId,
      ])
      .then((data) => {
        let noticeDetails = {
          Category: data.rows.item(0).Category,
          ID: data.rows.item(0).ID,
          NBody: data.rows.item(0).NBody,
          NDate: data.rows.item(0).NDate,
          NOnlineNotificationId: data.rows.item(0).NOnlineNotificationId,
          NStatus: data.rows.item(0).NStatus,
          NTime: data.rows.item(0).NTime,
          sId: data.rows.item(0).sId,
        };

        return noticeDetails;
      });
  }

  updateCMSNotificationMessage(sid) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `UPDATE CMSNotificationMessage SET NStatus = ? WHERE ID = ?`,
        ["Read", sid]
      );
  }

  insertIntoOnlineTestNotifications(testDetails, testId) {
    console.log("testDetails", testDetails.title);
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM onlineTestNotifications WHERE ArrengeTestId=?`,
        [testDetails.arrengeTestId]
      )
      .then((data) => {
        if (data.rows.length == 0) {
          this.databaseService
            .getDataBase()
            .executeSql(
              `INSERT INTO onlineTestNotifications(Title, TestDate, EndDate, StartTime, EndTime, Duration, TestPaperId, SId, Status, 
               ArrengeTestId,CetCorrect,CetInCorrect,NeetCorrect,NeetInCorrect,JeeCorrect,JeeInCorrect,JeeNewCorrect,JeeNewInCorrect,
               TestObj,TestType) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
              [
                testDetails.title,
                testDetails.testDate,
                testDetails.EndDate,
                testDetails.startTime,
                testDetails.endTime,
                testDetails.duration,
                testDetails.testPaperId,
                testDetails.sid,
                testId == null ? "Unread" : "read",
                testDetails.arrengeTestId,
                testDetails.CetCorrect,
                testDetails.CetInCorrect,
                testDetails.NeetCorrect,
                testDetails.NeetInCorrect,
                testDetails.JeeCorrect,
                testDetails.JeeInCorrect,
                testDetails.JeeNewCorrect,
                testDetails.JeeNewInCorrect,
                "TestObj",
                testDetails.TestType,
              ]
            )
            .then((_) =>
              this.databaseService
                .getDataBase()
                .executeSql(`SELECT * FROM onlineTestNotifications`, [])
                .then((data) => console.log("onlineTestNotifications", data))
            );
        }
      });
  }

  updateOnlineTestNotifications(testObj, id) {
    return this.databaseService
      .getDataBase()
      .executeSql(`UPDATE onlineTestNotifications SET TestObj=? WHERE ID=?`, [
        testObj,
        id,
      ]);
  }

  startTest(ID, status) {
    console.log("ID", ID);
    return this.databaseService
      .getDataBase()
      .executeSql(
        `UPDATE onlineTestNotifications SET Status=? ,TestObj=? WHERE ID=?`,
        [status, "Down", ID]
      );
  }

  insertIntoCMSNotificationMessage(
    Message,
    NDateOnline,
    NTimeOnline,
    SId,
    NotificationId,
    Category
  ) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSNotificationMessage WHERE Category=? AND NOnlineNotificationId=?`,
        [Category, NotificationId]
      )
      .then((data) => {
        if (data.rows.length == 0) {
          this.databaseService
            .getDataBase()
            .executeSql(
              `INSERT INTO CMSNotificationMessage(NBody, NDate, NTime, NStatus, sId, NOnlineNotificationId, Category) values(?,?,?,?,?,?,?)`,
              [
                Message,
                NDateOnline,
                NTimeOnline,
                "Unread",
                SId,
                NotificationId,
                Category,
              ]
            )
            .then((_) =>
              this.databaseService
                .getDataBase()
                .executeSql(`SELECT * FROM CMSNotificationMessage`, [])
                .then((data) => console.log("CMSNotificationMessage", data))
            );
        }
      });
  }

  insertIntoCMSNotificationMessageTest(
    Message,
    NDateOnline,
    NTimeOnline,
    SId,
    NotificationId
  ) {
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT * FROM CMSNotificationMessageTest WHERE NBody=?`, [
        Message,
      ])
      .then((data) => {
        if (data.rows.length == 0) {
          this.databaseService
            .getDataBase()
            .executeSql(
              `INSERT INTO CMSNotificationMessageTest(NBody, NDate, NTime, NStatus, sId, NOnlineNotificationId) values(?,?,?,?,?,?)`,
              [Message, NDateOnline, NTimeOnline, "Unread", SId, NotificationId]
            )
            .then((_) =>
              this.databaseService
                .getDataBase()
                .executeSql(`SELECT * FROM CMSNotificationMessageTest`, [])
                .then((data) => console.log("CMSNotificationMessageTest", data))
            );
        }
      });
  }

  getNotificationCount() {
    let notificationsCount: number = 0;
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSNotificationMessage WHERE NStatus = 'Unread'`,
        []
      )
      .then((data) => {
        this.storageService.generalNotificationsCount += data.rows.length;
        notificationsCount += data.rows.length;
        return this.databaseService
          .getDataBase()
          .executeSql(
            `SELECT * FROM onlineTestNotifications WHERE Status = 'Unread'`,
            []
          )
          .then((data1) => {
            this.storageService.testNotificationsCount += data1.rows.length;
            notificationsCount += data1.rows.length;
            return this.databaseService
              .getDataBase()
              .executeSql(
                `SELECT * FROM CMSNotificationMessageTest WHERE NStatus = 'Unread'`,
                []
              )
              .then((data2) => {
                this.storageService.generalNotificationsCount +=
                  data2.rows.length;
                notificationsCount += data2.rows.length;
                return notificationsCount;
              });
          });
      });
  }

  getNotifications() {
    console.log(this.storageService.userDetails.sId);
    return this.databaseService
      .getDataBase()
      .executeSql(`SELECT * FROM onlineTestNotifications order by ID desc`, [])
      .then((data) => {
        // console.log(data);
        let notifications: Notifications[] = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            notifications.push({
              Title: data.rows.item(i).Title,
              TestDate: data.rows.item(i).TestDate,
              StartTime: data.rows.item(i).StartTime,
              Duration: data.rows.item(i).Duration,
              TestPaperId: data.rows.item(i).TestPaperId,
              SId: data.rows.item(i).SId,
              Status: data.rows.item(i).Status,
              ID: data.rows.item(i).ID,
              ArrengeTestId: data.rows.item(i).ArrengeTestId,
              CetCorrect: data.rows.item(i).CetCorrect,
              CetInCorrect: data.rows.item(i).CetInCorrect,
              EndDate: data.rows.item(i).EndDate,
              EndTime: data.rows.item(i).EndTime,
              JeeCorrect: data.rows.item(i).JeeCorrect,
              JeeInCorrect: data.rows.item(i).JeeInCorrect,
              JeeNewCorrect: data.rows.item(i).JeeNewCorrect,
              JeeNewInCorrect: data.rows.item(i).JeeNewInCorrect,
              NeetCorrect: data.rows.item(i).NeetCorrect,
              NeetInCorrect: data.rows.item(i).NeetInCorrect,
              TestObj: data.rows.item(i).TestObj,
              TestType: data.rows.item(i).TestType,
            });
          }
        }
        return notifications;
      });
  }

  getMaxArrengeTestId() {
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT ArrengeTestId FROM onlineTestNotifications ORDER BY ID DESC LIMIT 1`,
        []
      )
      .then((data) => {
        console.log(data);
        if (data.rows.length > 0) return data.rows.item(0).ArrengeTestId;
        else return 0;
      });
  }

  deleteFromCMSNotificationId() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from CMSNotificationId`, []);
  }

  deleteFromCMSNotificationMessage() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from CMSNotificationMessage`, []);
  }

  deleteFromCMSNotificationMessageTest() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from CMSNotificationMessageTest`, []);
  }

  deleteFromNotification() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from notification`, []);
  }

  deleteFromOnlineTestNotifications() {
    return this.databaseService
      .getDataBase()
      .executeSql(`delete from onlineTestNotifications`, []);
  }
}
