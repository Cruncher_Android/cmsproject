import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastServiceService {

  constructor(private toastController: ToastController) { }

  async createToast(message, duration) {
    let toast = await this.toastController.create({
      animated: true,
      duration: duration,
      position: "bottom",
      message: message
    })
    await toast.present();
  }
}
