import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { FileInfoDPP } from "src/app/Models/fileInfo";


@Injectable({
  providedIn: 'root'
})
export class DailyPracticePaperServiceService {

  constructor(private databaseService: DatabaseService) {
    
   }

   insertIntoCMSDailyPracticePaper(
    description,
    date,
    fileName,
    attachmentDescription
  ) {
    this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO CMSDailyPracticePaper(Description, Date, FileName, AttachmentDescription) values(?,?,?,?)`,
        [description, date, fileName, attachmentDescription]
      );
  }

  selectFromCMSDailyPracticePapere() {
    let fileInfo: FileInfoDPP[] = [];
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSDailyPracticePaper ORDER BY ID DESC`,
        []
      )
      .then((data) => {
        for (let i = 0; i < data.rows.length; i++) {
          fileInfo.push({
            AttachmentDescription: data.rows.item(i).AttachmentDescription,
            Date: data.rows.item(i).Date,
            Description: data.rows.item(i).Description,
            FileName: data.rows.item(i).FileName,
          });
        }
        return fileInfo;
      });
  }

  deleteFromCMSDailyPracticePaper() {
    return this.databaseService
      .getDataBase()
      .executeSql(`DELETE FROM CMSDailyPracticePaper`, []);
  }
}
