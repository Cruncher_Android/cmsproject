import { TestBed } from '@angular/core/testing';

import { OfflineExamScheduleServiceService } from './offline-exam-schedule-service.service';

describe('OfflineExamScheduleServiceService', () => {
  let service: OfflineExamScheduleServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfflineExamScheduleServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
