import { TestBed } from '@angular/core/testing';

import { DailyPracticePaperServiceService } from './daily-practice-paper-service.service';

describe('DailyPracticePaperServiceService', () => {
  let service: DailyPracticePaperServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DailyPracticePaperServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
