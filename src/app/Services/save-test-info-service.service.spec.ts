import { TestBed } from '@angular/core/testing';

import { SaveTestInfoServiceService } from './save-test-info-service.service';

describe('SaveTestInfoServiceService', () => {
  let service: SaveTestInfoServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaveTestInfoServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
