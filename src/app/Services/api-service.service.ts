import { Injectable } from "@angular/core";
import { HTTP } from "@ionic-native/http/ngx";
import { StorageServiceService } from "./storage-service.service";

@Injectable({
  providedIn: "root",
})
export class ApiServiceService {
  // apiUrl = "http://gccms.crunchersoft.com/getregDataNew.aspx/";
  // fileUrl = "http://gccms.crunchersoft.com/";
  fileUrl = "http://test.onlineexamkatta.com/";
  apiUrl = "http://test.onlineexamkatta.com/getregDataNew.aspx/";

  constructor(
    private nativeHttp: HTTP,
    private storageService: StorageServiceService
  ) {}

  login(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}checkUserName`,
      { userName: userId },
      {}
    );
  }

  checkActiveStatus(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}checkActiveStatus`,
      { userId: userId },
      {}
    );
  }

  createPassword(userId, password) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}createPasswordForStudent`,
      { userId: userId, newPass: password },
      {}
    );
  }

  getUserDetails(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}sendingMessageStudentApp`,
      { userId: userId },
      {}
    );
  }

  setPlayerId(userId, playerId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}setPlayerIdCMSStudentAppData`,
      { userId: userId, playerId: playerId },
      {}
    );
  }

  getAttendanceData() {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getCMSAttendanceData`,
      {
        SId: this.storageService.userDetails.sId,
        ClassId: this.storageService.userDetails.classId,
        selectedBatches: this.storageService.userDetails.batchId,
        branchId: this.storageService.userDetails.branchId,
      },
      {}
    );
  }

  getAttendanceCount(sid, classId, selectedBatches, branchId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getCMSAttendanceDataCount`,
      {
        SId: sid,
        ClassId: classId,
        selectedBatches: selectedBatches,
        branchId: branchId,
      },
      {}
    );
  }

  getFeesData(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getCMSStudentFeesData`,
      { stud_id: userId },
      {}
    );
  }

  getFeesHistory(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getCMSStudentFeesHistoryData`,
      { stud_id: userId },
      {}
    );
  }

  getNotificationsOnline(
    branchId,
    classId,
    selectedBatches,
    NotificationId,
    pdfUploadId,
    arrengeTestId,
    MaxOfflineTestPaperId,
    MaxOfflineTestStudentMarksId,
    userId,
    selectedSubjects,
    clientId,
    studentName,
    receiveCode,
    tokenId
  ) {
    console.log(
      branchId,
      classId,
      selectedBatches,
      NotificationId,
      pdfUploadId,
      arrengeTestId,
      MaxOfflineTestPaperId,
      MaxOfflineTestStudentMarksId,
      userId,
      selectedSubjects,
      studentName
    );
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetCMSStudentNotification`,
      {
        branchId: branchId,
        classId: classId,
        selectedbatches: selectedBatches,
        notificationMaxId: NotificationId,
        pdfUploadId: pdfUploadId,
        arrengeTestId: arrengeTestId,
        maxOfflineTestPaperId: MaxOfflineTestPaperId,
        maxOfflineTestStudentMarksId: MaxOfflineTestStudentMarksId,
        userId: userId,
        selectedSubjects: selectedSubjects,
        ClientId: clientId,
        studentName: studentName,
        receiveCode: receiveCode,
        TokenId: tokenId,
      },
      {}
    );
  }

  getTestObj(testPaperId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.get(
      `${this.fileUrl}Api/PaperAPi/${testPaperId}`,
      {},
      {}
    );
  }

  sendTestDetails(testDetails) {
    console.log("api test details", testDetails);
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}PostResultOfOnlineTest`,
      { testDetails: JSON.stringify(testDetails) },
      {}
    );
  }

  getResult(userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetStudentResults`,
      { userId: userId },
      {}
    );
  }

  setToken(userId, token) {
    console.log("userId", userId, "tokenId", token);
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}saveTokanId`,
      { userId: userId, tokenId: token },
      {}
    );
  }

  getPdfListSubjectWise(branchId, classId, subjectId, pdfType) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}${pdfType}`,
      { branchId: branchId, classId: classId, subjectId: subjectId },
      {}
    );
  }

  getTimeTable(branchId, classId, batchId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetStudentTimeTable`,
      { branchId: branchId, classId: classId, batchId: batchId },
      {}
    );
  }

  getDailyPracticePaper(branchId, classId, batchId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetStudentDailyPracticePaper`,
      { branchId: branchId, classId: classId, batchId: batchId },
      {}
    );
  }

  getVideo(branchId, classId, subjectId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetStudentVideoCourse`,
      { branchId: branchId, classId: classId, subjectId: subjectId },
      {}
    );
  }

  getCMSOnlineTestNotification(clientId, userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetCMSOnlineTestNotification`,
      { ClientId: clientId, UserId: userId },
      {}
    );
  }

  getCMSOnlineTestRecived(userId, clientId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetCMSOnlineTestRecived`,
      { userId: userId, ClientId: clientId },
      {}
    );
  }

  GetCMReceivedTestNotification(arrangeTestId, userId, clientId, recivedCode) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetCMReceivedTestNotification`,
      {
        arrengeTestId: arrangeTestId,
        userId: userId,
        ClientId: clientId,
        receiveCode: recivedCode,
      },
      {}
    );
  }

  getCMSLatestOnlineTestRecived(userId, clientId, arrangeTestId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}GetCMSLatestOnlineTestRecived`,
      { userId: userId, ClientId: clientId, ArrengeTestId: arrangeTestId },
      {}
    );
  }

  getAboutUsDetails() {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getClientData`,
      {
        userId: this.storageService.userDetails.userId,
      },
      {}
    );
  }

  setSecurityCode(clientId, userId, securityCode) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}setSecurityCode`,
      { clientId: clientId, userId: userId, securityCode: securityCode },
      {}
    );
  }

  removeSecurityCode(clientId, userId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}RemoveSecurityCode`,
      { clientId: clientId, userId: userId, loginStatus: "loggedOut" },
      {}
    );
  }

  getSavedTests(userId, clientId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}sendOnlineTestResult`,
      { userId: userId, clientId: clientId },
      {}
    );
  }

  // getSavedTestsQuestions(testPaperId) {
  //   this.nativeHttp.setDataSerializer("json");
  //   return this.nativeHttp.get(
  //     `http://test.onlineexamkatta.com/api/paperapi/${testPaperId}`,
  //     {},
  //     {}
  //   );
  // }

  sendOTPThroughMail(emailId, code) {
    console.log("Email", emailId);
    console.log("Code", code);
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}sendEmail`,
      { EmailId: emailId, Code: code },
      {}
    );
  }

  saveDeviceLogin(userId, clientId, deviceId, loginStatus) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}saveDeviceLogin`,
      {
        userId: userId,
        clientId: clientId,
        deviceId: deviceId,
        loginStatus: loginStatus,
      },
      {}
    );
  }

  APPPostResultOfOnlineTest(savedTestDetails, userId, clientId) {
    console.log(
      "savedTestDetailsApi",
      JSON.stringify({
        ...savedTestDetails,
        userId,
        clientId,
      })
    );
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}APPPostResultOfOnlineTest`,
      {
        savedTestDetails: JSON.stringify({
          ...savedTestDetails,
          userId,
          clientId,
        }),
      },
      {}
    );
  }

  getCMSOfflineTestNotification(userDetails, maxOfflineTestPaperId) {
    this.nativeHttp.setDataSerializer("json");
    return this.nativeHttp.post(
      `${this.apiUrl}getCMSOfflineTestNotification`,
      {
        maxOfflineTestPaperId: maxOfflineTestPaperId,
        clientId: userDetails.clientId,
        branchId: userDetails.branchId,
        classId: userDetails.classId,
        selectedbatches: userDetails.selectedBatches,
        selectedSubjects: userDetails.selectedSubjects,
      },
      {}
    );
  }
}
