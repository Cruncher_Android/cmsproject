import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { attendanceDetails } from "../Models/attendanceInfo";

@Injectable({
  providedIn: "root",
})
export class AttendanceServiceService {
  constructor(private databaseService: DatabaseService) {}

  deleteFromCMSAttendance() {
    return this.databaseService
      .getDataBase()
      .executeSql("DELETE FROM CMSAttendance", []);
  }

  insertIntoCMSAttendance(
    adate,
    attStatus,
    SubjectName,
    BatchName,
    InTime,
    OutTime,
    MonthOfDate
  ) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        "INSERT INTO CMSAttendance(ADate, status, subjectName, batchName, inTime, outTime, monthOfDate) values(?,?,?,?,?,?,?)",
        [adate, attStatus, SubjectName, BatchName, InTime, OutTime, MonthOfDate]
      );
    // .then((_) =>
    //   this.databaseService
    //     .getDataBase()
    //     .executeSql("select * from CMSAttendance", [])
    //     .then((data) => console.log("CMSAttendance", data))
    // );
  }

  deleteFromCMSAttendanceCount() {
    return this.databaseService
      .getDataBase()
      .executeSql("DELETE FROM CMSAttendanceCount", []);
  }

  insertIntoCMSAttendanceCount(dataLength, monthNm, totalCount, presentCount) {
    return this.databaseService
      .getDataBase()
      .executeSql(
        "INSERT INTO CMSAttendanceCount(id, monthName, totalCount, presentCount) values(?,?,?,?)",
        [dataLength, monthNm, totalCount, presentCount]
      )
      .then((_) =>
        this.databaseService
          .getDataBase()
          .executeSql("select * from CMSAttendanceCount", [])
          .then((data) => console.log("CMSAttendanceCount", data))
      );
  }

  getAttendance() {
    return this.databaseService
      .getDataBase()
      .executeSql("SELECT * FROM CMSAttendance ORDER BY ADate DESC", [])
      .then((data) => {
        let attendanceData: attendanceDetails[] = [];
        for (let i = 0; i < data.rows.length; i++) {
          attendanceData.push({
            date: data.rows.item(i).ADate,
            status: data.rows.item(i).status,
            subjectName: data.rows.item(i).subjectName,
            inTime: data.rows.item(i).inTime,
            outTime: data.rows.item(i).outTime,
            batchName: data.rows.item(i).batchName,
            monthOfDate: data.rows.item(i).monthOfDate,
          });
        }
        return attendanceData;
      });
  }

  getDistinctSubjects() {
    return this.databaseService
      .getDataBase()
      .executeSql("SELECT DISTINCT subjectName FROM CMSAttendance", [])
      .then((data) => {
        let subjects = [];
        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            subjects.push(data.rows.item(0).subjectName);
          }
        }
        return subjects;
      });
  }
}
