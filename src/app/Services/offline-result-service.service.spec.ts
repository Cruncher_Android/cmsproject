import { TestBed } from '@angular/core/testing';

import { OfflineResultServiceService } from './offline-result-service.service';

describe('OfflineResultServiceService', () => {
  let service: OfflineResultServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfflineResultServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
