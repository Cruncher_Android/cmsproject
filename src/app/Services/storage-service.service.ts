import { Injectable } from "@angular/core";
import { UserInfo, UserDetails } from "./../Models/userInfo";
import { Notifications } from "src/app/Models/notificationInfo";

@Injectable({
  providedIn: "root",
})
export class StorageServiceService {
  userInfo: UserInfo;
  forgotPassword: boolean;
  firstTimeLogin: boolean;
  userDetails: UserDetails = {
    parentName: "",
    studentName: "",
    parentContact: "",
    className: "",
    sId: "",
    birthDate: "",
    classId: "",
    selectedSubjects: "",
    studentContact: "",
    branchName: "",
    email: "",
    branchId: "",
    onlineNotificationId: "",
    onlineTestId: "",
    batchId: "",
    subjectName: "",
    batchName: "",
    pdfUploadId: "",
    maxOfflineTestPaperId: "",
    maxOfflineTestStudentMarksId: "",
    clientId: "",
    userId: "",
    status: "",
    schoolName: "",
    doj: "",
    photoPath: "",
  };
  testNotificationsCount: number = 0;
  generalNotificationsCount: number = 0;
  notification: Notifications;
  page: string = "";
  pdftype: string = "";
  pdftypeget: string = "";
  tokenId: string = "";
  password: string = "";
  testExpired: string = "";
  deviceId: string = "";
  // instituteName: string = 'P K Foundation, Pune(Chakan)';
  instituteName: string = "";
  constructor() {}
}
