import { Injectable } from "@angular/core";
import { DatabaseService } from "./database.service";
import { FileInfoTimeTable } from "src/app/Models/fileInfo";

@Injectable({
  providedIn: "root",
})
export class TimeTableServiceService {
  constructor(private databaseService: DatabaseService) {}

  insertIntoCMSTimeTable(
    description,
    date,
    fileName,
    category,
    attachmentDescription
  ) {
    this.databaseService
      .getDataBase()
      .executeSql(
        `INSERT INTO CMSTimeTable(Description, Date, FileName, Category, AttachmentDescription) values(?,?,?,?,?)`,
        [description, date, fileName, category, attachmentDescription]
      );
  }

  selectFromCMSTimeTable(category) {
    let fileInfo: FileInfoTimeTable[] = [];
    return this.databaseService
      .getDataBase()
      .executeSql(
        `SELECT * FROM CMSTimeTable WHERE Category=? ORDER BY ID DESC`,
        [category]
      )
      .then((data) => {
        for (let i = 0; i < data.rows.length; i++) {
          fileInfo.push({
            AttachmentDescription: data.rows.item(i).AttachmentDescription,
            Category: data.rows.item(i).Category,
            Date: data.rows.item(i).Date,
            Description: data.rows.item(i).Description,
            FileName: data.rows.item(i).FileName,
          });
        }
        return fileInfo;
      });
  }

  deleteFromCMSTimeTable() {
    return this.databaseService
      .getDataBase()
      .executeSql(`DELETE FROM CMSTimeTable`, []);
  }
}
