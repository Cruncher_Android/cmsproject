import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { ApiServiceService } from "./api-service.service";
import { StorageServiceService } from "./storage-service.service";
import { DatabaseService } from "./database.service";
import { UserInfo, UserDetails } from "src/app/Models/userInfo";
import { Router } from "@angular/router";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AlertServiceService } from "./alert-service.service";
import { FCM } from "cordova-plugin-fcm-with-dependecy-updated/ionic/ngx";
import { NotificationServiceService } from "./notification-service.service";
import { TestDetails } from "../Models/testDetails";
import { LoadingServiceService } from "./loading-service.service";
import { SavedTestDetails } from "./../Models/savedTestDetails";
import { QuestionInfo } from "../Models/questionInfo";
import { SaveTestInfoServiceService } from "./save-test-info-service.service";
import { LoadingController } from "@ionic/angular";
import { JsonPipe } from "@angular/common";

@Injectable({
  providedIn: "root",
})
export class UserDetailsService {
  // testDetails: TestDetails[] = [];
  constructor(
    private fcm: FCM,
    private router: Router,
    private storage: Storage,
    private statusBar: StatusBar,
    private loadingController: LoadingController,
    private apiService: ApiServiceService,
    private storageService: StorageServiceService,
    private databaseService: DatabaseService,
    private alertService: AlertServiceService,
    private notificationService: NotificationServiceService,
    private loadingService: LoadingServiceService,
    private saveTestInfoServiceService: SaveTestInfoServiceService
  ) {}

  getUserDetails() {
    this.apiService
      .getUserDetails(this.storageService.userInfo.UserId)
      .then((data) => console.log(JSON.parse(data.data)));
  }

  insertIntoCMSLogStatus() {
    let userInfo: UserInfo = this.storageService.userInfo;
    return this.databaseService
      .getDataBase()
      .executeSql("SELECT * FROM CMSlogStatus WHERE userId=?", [
        userInfo.UserId,
      ])
      .then((data) => {
        if (data.rows.length == 0) {
          this.databaseService
            .getDataBase()
            .executeSql(
              "INSERT INTO CMSlogStatus(userId, sId, classId, logStatus) values(?,?,?,?)",
              [userInfo.UserId, userInfo.SID, userInfo.ClassId, 1]
            )
            .then((_) => {
              this.storage.ready().then((ready) => {
                if (ready) {
                  this.storage.set("insertQuestions", 1);
                  this.setPlayerId();
                }
              });
            });
        }
      });
  }

  deleteFromCMSLogStatus() {
    return this.databaseService
      .getDataBase()
      .executeSql("Delete from CMSlogStatus", []);
  }

  insertIntoCMSRegistrationDetails(data) {
    let userInfo: UserInfo = this.storageService.userInfo;
    let userData = [];
    userData = [
      "",
      data.ParentContact,
      data.StudentContact,
      "no",
      userInfo.UserId,
      data.StudentName,
      "logout",
      data.ClassName,
      data.SId,
      data.ClassId,
      data.SelectedSubjects,
      data.SubjectName,
      "no",
      data.Email,
      "no",
      data.BranchName,
      "no",
      data.BranchId,
      data.NotificationId,
      data.BatchId,
      data.BatchName,
      data.PdfUploadId,
      data.OnlineTestId,
      data.MaxOfflineTestPaperId,
      data.MaxOfflineTestStudentMarksId,
      data.ClientId,
    ];

    console.log("user info", userInfo);
    return this.databaseService
      .getDataBase()
      .executeSql("SELECT * FROM CMSRegistrationDetails WHERE userId=?", [
        userInfo.UserId,
      ])
      .then((data) => {
        console.log("data in database", data);
        if (data.rows.length == 0) {
          this.databaseService
            .getDataBase()
            .executeSql(
              `INSERT INTO CMSRegistrationDetails(parentName, parentNo, studentNo, DOB, userId, studentName, status, className, sId, 
               classId, selectedSubjects, SubjectName, schoolName, Email, DOJ, BranchName, PhotoPath, branchId, ROnlineNotificationId, 
               BatchId, BatchName, RPdfUploadId, ROnlineTestNotificationId, MaxOfflineTestPaperId, MaxOfflineTestStudentMarksId,ClientId) 
               values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
              userData
            )
            .then((_) => {
              this.router.navigateByUrl("/dashboard-page/home-page");
              this.statusBar.backgroundColorByHexString("#0d4fc3");
              this.statusBar.styleBlackOpaque();
              this.setLoginStatus();
              // this.selectFromCMSRegistrationDetails();
              this.saveDeviceLogin(
                this.storageService.userInfo.UserId,
                this.storageService.userInfo.ClientId,
                this.storageService.deviceId,
                "loggedIn"
              );
              this.setFCMToken();
              this.getTestList(
                this.storageService.userInfo.ClientId,
                this.storageService.userInfo.UserId
              );
              // this.databaseService
              //   .getDataBase()
              //   .executeSql("SELECT * FROM CMSRegistrationDetails", []).then(data => console.log('data in database', data))
            });
        } else {
          this.router.navigateByUrl("/dashboard-page/home-page");
          this.statusBar.backgroundColorByHexString("#0d4fc3");
          this.statusBar.styleBlackOpaque();
          this.setLoginStatus();
          // this.selectFromCMSRegistrationDetails();
          this.saveDeviceLogin(
            this.storageService.userInfo.UserId,
            this.storageService.userInfo.ClientId,
            this.storageService.deviceId,
            "loggedIn"
          );
          this.setFCMToken();
          this.getTestList(
            this.storageService.userInfo.ClientId,
            this.storageService.userInfo.UserId
          );
        }
      });
  }

  selectFromCMSRegistrationDetails() {
    return this.databaseService
      .getDataBase()
      .executeSql("select * from CMSRegistrationDetails", [])
      .then((data) => {
        let userDetails: UserDetails = {
          parentName: data.rows.item(0).parentName,
          studentName: data.rows.item(0).studentName,
          parentContact: data.rows.item(0).parentNo,
          className: data.rows.item(0).className,
          sId: data.rows.item(0).sId,
          birthDate: data.rows.item(0).DOB,
          classId: data.rows.item(0).classId,
          selectedSubjects: data.rows.item(0).selectedSubjects,
          studentContact: data.rows.item(0).studentNo,
          branchName: data.rows.item(0).BranchName,
          email: data.rows.item(0).Email,
          branchId: data.rows.item(0).branchId,
          onlineNotificationId: data.rows.item(0).ROnlineNotificationId,
          onlineTestId: data.rows.item(0).ROnlineTestNotificationId,
          batchId: data.rows.item(0).BatchId,
          subjectName: data.rows.item(0).SubjectName,
          batchName: data.rows.item(0).BatchName,
          pdfUploadId: data.rows.item(0).RPdfUploadId,
          maxOfflineTestPaperId: data.rows.item(0).MaxOfflineTestPaperId,
          maxOfflineTestStudentMarksId: data.rows.item(0)
            .MaxOfflineTestStudentMarksId,
          clientId: data.rows.item(0).ClientId,
          userId: data.rows.item(0).userId,
          status: data.rows.item(0).status,
          schoolName: data.rows.item(0).schoolName,
          doj: data.rows.item(0).DOJ,
          photoPath: data.rows.item(0).PhotoPath,
        };
        return userDetails
      });
  }

  deleteFromCMSRegistrationDetails() {
    return this.databaseService
      .getDataBase()
      .executeSql("delete from CMSRegistrationDetails", []);
  }

  setPlayerId() {
    let userId = this.storageService.userInfo.UserId;
    this.apiService.setPlayerId(userId, userId).then(
      (data) => {
        // console.log(JSON.parse(data.data).d);
        let length = JSON.parse(data.data).d.length;
        let resultData = JSON.parse(data.data).d[0].result;
        // console.log(length, resultData)
        // if (length > 0) {
        //   if (resultData == "not found") {
        //   } else if (
        //     resultData == "set" ||
        //     resultData == "already set"
        //   ) {
        //   }
        // }
      },
      (err) => console.log(err)
    );
  }

  getStudentAccess(userId) {
    this.apiService.getUserDetails(userId).then((data) => {
      console.log("in ts file", JSON.parse(data.data).d);
      let length = JSON.parse(data.data).d.length;
      if (JSON.parse(data.data).d[0].UserId != null) {
        if (length > 0) {
          if (JSON.parse(data.data).d[0].IsActive == "True") {
            JSON.parse(data.data).d.map((data) => {
              this.storage.ready().then((ready) => {
                if (ready) {
                  this.storage.set("branchId", data.BatchId);
                }
              });
              this.insertIntoCMSRegistrationDetails(data);
            });
          } else {
            this.alertService.createAlert("Student is not activated by class");
          }
        }
      } else {
        this.alertService.createAlert("You are inactive");
      }
    });
  }

  setLoginStatus() {
    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.set("userLoggedIn", true);
      }
    });
  }

  saveDeviceLogin(userId, clientId, deviceId, loginStatus) {
    this.apiService
      .saveDeviceLogin(userId, clientId, deviceId, loginStatus)
      .then((data) => console.log(JSON.parse(data.data)));
  }

  setFCMToken() {
    this.fcm.getToken().then((token) => {
      console.log(token);
      this.apiService
        .setToken(this.storageService.userInfo.UserId.trim(), token)
        .then((data) => {
          console.log(data.data);
          this.storageService.tokenId = token;
          this.storage.set("token", token);
        });
    });
  }

  getTestList(clientId, userId) {
    // var dummy = 0;
    this.apiService
      .getCMSOnlineTestNotification(clientId, userId)
      .then((allTests) => {
        console.log("all tests", JSON.parse(allTests.data));
        this.apiService
          .getCMSOnlineTestRecived(userId, clientId)
          .then((recivedTests) => {
            console.log("recived tests", JSON.parse(recivedTests.data));
            this.getTestsDetails(
              JSON.parse(allTests.data).d,
              JSON.parse(recivedTests.data).d
            );
          });
      });
  }

  getTestsDetails(allTests, recivedTests) {
    let testDetails: TestDetails[] = [];
    let maxPaperId: string = allTests[allTests.length - 1].ArrengeTestId;
    this.storage.set("maxTestPaperId", maxPaperId);
    allTests.map((test) => {
      var Message = test.Message;
      var title = Message.split("$^$")[0];
      var TestType = Message.split("$^$")[1].split("TestType:")[1];
      var CetCorrect = Message.split("$^$")[2].split("CetCorrect:")[1];
      var CetInCorrect = Message.split("$^$")[3].split("CetInCorrect:")[1];
      var NeetCorrect = Message.split("$^$")[4].split("NeetCorrect:")[1];
      var NeetInCorrect = Message.split("$^$")[5].split("NeetInCorrect:")[1];
      var JeeCorrect = Message.split("$^$")[6].split("JeeCorrect:")[1];
      var JeeInCorrect = Message.split("$^$")[7].split("JeeInCorrect:")[1];
      var JeeNewCorrect = Message.split("$^$")[8].split("JeeNewCorrect:")[1];
      var JeeNewInCorrect = Message.split("$^$")[9].split(
        "JeeNewInCorrect:"
      )[1];
      var testDate = Message.split("$^$")[10].split("Date:")[1];
      var EndDate = Message.split("$^$")[11].split("EndDate:")[1];
      var startTime = Message.split("$^$")[12].split("Start Time:")[1];
      var endTime = Message.split("$^$")[13].split("End Time:")[1];
      var duration = Message.split("$^$")[14].split("Duration:")[1];
      var testPaperId = Message.split("$^$")[15].split("TestPaperId:")[1];

      if (testDetails.length == 0) {
        testDetails.push({
          CetCorrect: CetCorrect,
          CetInCorrect: CetInCorrect,
          EndDate: EndDate,
          JeeCorrect: JeeCorrect,
          JeeInCorrect: JeeInCorrect,
          JeeNewCorrect: JeeNewCorrect,
          JeeNewInCorrect: JeeNewInCorrect,
          NeetCorrect: NeetCorrect,
          NeetInCorrect: NeetInCorrect,
          TestType: TestType,
          arrengeTestId: test.ArrengeTestId,
          duration: duration,
          endTime: endTime,
          sid: this.storageService.userInfo.SID,
          startTime: startTime,
          status: "",
          testDate: testDate,
          testPaperId: testPaperId,
          title: title,
        });
        this.storeToDatabase(testDetails[0], recivedTests);
      } else {
        if (
          testDetails.find(
            (testDetail) =>
              testDetail.testPaperId == testPaperId &&
              testDetail.testDate == testDate
          ) == null
        ) {
          testDetails.push({
            CetCorrect: CetCorrect,
            CetInCorrect: CetInCorrect,
            EndDate: EndDate,
            JeeCorrect: JeeCorrect,
            JeeInCorrect: JeeInCorrect,
            JeeNewCorrect: JeeNewCorrect,
            JeeNewInCorrect: JeeNewInCorrect,
            NeetCorrect: NeetCorrect,
            NeetInCorrect: NeetInCorrect,
            TestType: TestType,
            arrengeTestId: test.ArrengeTestId,
            duration: duration,
            endTime: endTime,
            sid: this.storageService.userInfo.SID,
            startTime: startTime,
            status: "",
            testDate: testDate,
            testPaperId: testPaperId,
            title: title,
          });
          let dummyDetails: TestDetails = {
            CetCorrect: CetCorrect,
            CetInCorrect: CetInCorrect,
            EndDate: EndDate,
            JeeCorrect: JeeCorrect,
            JeeInCorrect: JeeInCorrect,
            JeeNewCorrect: JeeNewCorrect,
            JeeNewInCorrect: JeeNewInCorrect,
            NeetCorrect: NeetCorrect,
            NeetInCorrect: NeetInCorrect,
            TestType: TestType,
            arrengeTestId: test.ArrengeTestId,
            duration: duration,
            endTime: endTime,
            sid: this.storageService.userInfo.SID,
            startTime: startTime,
            status: "",
            testDate: testDate,
            testPaperId: testPaperId,
            title: title,
          };
          this.storeToDatabase(dummyDetails, recivedTests);
        }
      }
    });
  }

  storeToDatabase(testDetails, recivedTests) {
    var testId = recivedTests.find(
      (recivedTest) => recivedTest.ArrengeTestId == testDetails.arrengeTestId
    );

    console.log("testId", testId);

    this.notificationService.insertIntoOnlineTestNotifications(
      testDetails,
      testId
    );
  }

  getSavedTests(userId, clientId) {
    console.log("userId", userId);
    console.log("clientId", clientId);
    this.apiService.getSavedTests(userId, clientId).then((data) => {
      let savedTests: SavedTestDetails;
      let results: any[] = JSON.parse(data.data).d;
      console.log("result", results);
      results.map((result) => {
        this.saveTestInfoServiceService.insertIntoTable(result);
      });
    });
  }
}
