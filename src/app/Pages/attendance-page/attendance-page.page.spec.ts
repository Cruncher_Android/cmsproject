import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttendancePagePage } from './attendance-page.page';

describe('AttendancePagePage', () => {
  let component: AttendancePagePage;
  let fixture: ComponentFixture<AttendancePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AttendancePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
