import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendancePagePage } from './attendance-page.page';

const routes: Routes = [
  {
    path: '',
    component: AttendancePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendancePagePageRoutingModule {}
