import { Component, OnInit, ViewChild } from "@angular/core";
import { Chart } from "chart.js";
import { DatabaseService } from "./../../Services/database.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { AttendanceServiceService } from "./../../Services/attendance-service.service";
import { attendanceDetails } from "./../../Models/attendanceInfo";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";

@Component({
  selector: "app-attendance-page",
  templateUrl: "./attendance-page.page.html",
  styleUrls: ["./attendance-page.page.scss"],
})
export class AttendancePagePage implements OnInit {
  @ViewChild("pieChart") pieChart1;
  pie: any;
  userId: string = "";
  sid: string = "";
  classId: string = "";
  selectedBatches: string = "";
  branchId: string = "";
  selectedMonth: string = "0";
  selectedSubject: string = "0";
  selectedStatus: string = "0";
  attendanceData: attendanceDetails[] = [];
  attendanceDataLocal: attendanceDetails[] = [];
  statusWiseAttendanceData: attendanceDetails[] = [];
  subjectWiseAttendanceData: attendanceDetails[] = [];

  subjects = [];
  constructor(
    private databaseService: DatabaseService,
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private attendanceService: AttendanceServiceService,
    private internetService: InternetServiceService,
    private toastService: ToastServiceService
  ) {}

  ngOnInit() {
    this.userId = this.storageService.userDetails.userId;
    this.sid = this.storageService.userDetails.sId;
    this.classId = this.storageService.userDetails.classId;
    this.selectedBatches = this.storageService.userDetails.batchId;
    this.branchId = this.storageService.userDetails.branchId;
    if (this.internetService.networkConnected) this.getAttendanceData();
    else {
      this.getMoreStudAttendanceData();
      this.getDistinctSubjects();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
  }

  getAttendanceData() {
    this.apiService.getAttendanceData().then(
      (result) => {
        console.log(JSON.parse(result.data).d[0]);
        let data = JSON.parse(result.data);
        let dataLength: number = 0;
        if (data.d.length > 0) {
          this.attendanceService.deleteFromCMSAttendance().then((_) => {
            for (var i = 0; i < data.d.length; i++) {
              var adate = data.d[i].ADate;
              var SubjectName = data.d[i].SubjectName;
              var BatchName = data.d[i].BatchName;
              var InTime = data.d[i].InTime;
              var OutTime = data.d[i].OutTime;
              var MonthOfDate = data.d[i].Month;
              var attStatus = data.d[i].Status;
              dataLength = i + 1;
              this.attendanceService.insertIntoCMSAttendance(
                adate,
                attStatus,
                SubjectName,
                BatchName,
                InTime,
                OutTime,
                MonthOfDate
              );
            }
            if (dataLength == data.d.length) {
              this.getAttendanceCount(
                this.sid,
                this.classId,
                this.userId,
                this.selectedBatches,
                this.branchId
              );
            }
          });
        } else if (data.d.length == 0) {
          this.attendanceService.deleteFromCMSAttendance().then((_) => {
            this.getAttendanceCount(
              this.sid,
              this.classId,
              this.userId,
              this.selectedBatches,
              this.branchId
            );
          });
        }
      },
      (err) => {
        this.getAttendanceCount(
          this.sid,
          this.classId,
          this.userId,
          this.selectedBatches,
          this.branchId
        );
      }
    );
  }

  getAttendanceCount(sid, classId, userId, selectedBatches, branchId) {
    this.apiService
      .getAttendanceCount(sid, classId, selectedBatches, branchId)
      .then(
        (result) => {
          // console.log(JSON.parse(result.data));
          let data = JSON.parse(result.data);
          let dataLength: number = 0;
          if (data.d.length > 0) {
            let totalCount = data.d[0].TotalCount;
            let presentCount = data.d[0].PresentCount;
            this.attendanceService.deleteFromCMSAttendanceCount().then((_) => {
              for (var i = 0; i < totalCount.length; i++) {
                dataLength = i + 1;
                var monthNm = "";
                if (dataLength == 1) monthNm = "Jan";
                else if (dataLength == 2) monthNm = "Feb";
                else if (dataLength == 3) monthNm = "Mar";
                else if (dataLength == 4) monthNm = "Apr";
                else if (dataLength == 5) monthNm = "May";
                else if (dataLength == 6) monthNm = "Jun";
                else if (dataLength == 7) monthNm = "Jul";
                else if (dataLength == 8) monthNm = "Aug";
                else if (dataLength == 9) monthNm = "Sept";
                else if (dataLength == 10) monthNm = "Oct";
                else if (dataLength == 11) monthNm = "Nov";
                else if (dataLength == 12) monthNm = "Dec";
                this.attendanceService.insertIntoCMSAttendanceCount(
                  dataLength,
                  monthNm,
                  totalCount[i],
                  presentCount[i]
                );
              }
              this.getMoreStudAttendanceData();
              this.getDistinctSubjects();
              // this.getTopFiveMonthAttendance();
            });
          } else if (data.d.length == 0) {
            this.getMoreStudAttendanceData();
            this.getDistinctSubjects();
            // this.getTopFiveMonthAttendance();
          }
        },
        (err) => {
          this.getMoreStudAttendanceData();
          this.getDistinctSubjects();
          // this.getTopFiveMonthAttendance();
        }
      );
  }

  getMoreStudAttendanceData() {
    this.attendanceService.getAttendance().then((data) => {
      this.attendanceDataLocal = data;
      this.attendanceData = [...this.attendanceDataLocal];
      this.getTopFiveMonthAttendance();
      console.log("attendance data", this.attendanceData);
    });
  }

  getDistinctSubjects() {
    this.attendanceService.getDistinctSubjects().then((data) => {
      this.subjects = data.filter((d) => d != null);
      console.log("subjects", this.subjects);
    });
  }

  onSelectChange() {
    // console.log("selected status", this.selectedStatus);
    // console.log("Selected Month", this.selectedMonth);
    if (
      this.selectedMonth == "0" &&
      this.selectedStatus != "0" &&
      this.selectedSubject == "0"
    )
      this.getOnlyStatusWiseAttendance(this.selectedStatus);
    else if (
      this.selectedMonth != "0" &&
      this.selectedStatus == "0" &&
      this.selectedSubject == "0"
    )
      this.getOnlyMonthWiseAttendance(this.selectedMonth);
    else if (
      this.selectedMonth == "0" &&
      this.selectedStatus == "0" &&
      this.selectedSubject != "0"
    )
      this.getOnlySubjectWiseAttendance(this.selectedSubject);
    else if (
      this.selectedMonth != "0" &&
      this.selectedStatus != "0" &&
      this.selectedSubject == "0"
    )
      this.getMonthAndStatusWiseAttendance(
        this.selectedMonth,
        this.selectedStatus
      );
    else if (
      this.selectedMonth != "0" &&
      this.selectedStatus == "0" &&
      this.selectedSubject != "0"
    )
      this.getMonthAndSubjectWiseAttendance(
        this.selectedMonth,
        this.selectedSubject
      );
    else if (
      this.selectedMonth == "0" &&
      this.selectedStatus != "0" &&
      this.selectedSubject != "0"
    )
      this.getStatusAndSubjectWiseAttendance(
        this.selectedStatus,
        this.selectedSubject
      );
    else
      this.getStatusSubjectMonthWiseAttendance(
        this.selectedStatus,
        this.selectedSubject,
        this.selectedMonth
      );
  }

  getOnlyStatusWiseAttendance(selectedStatus) {
    this.attendanceData = [];
    if (selectedStatus == "Both")
      this.attendanceData = [...this.attendanceDataLocal];
    else
      this.attendanceData = this.attendanceDataLocal.filter(
        (data) => data.status == selectedStatus
      );
  }

  getOnlyMonthWiseAttendance(selectedMonth) {
    this.attendanceData = [];
    if (selectedMonth == "13")
      this.attendanceData = [...this.attendanceDataLocal];
    else
      this.attendanceData = this.attendanceDataLocal.filter(
        (data) => data.monthOfDate == selectedMonth
      );
  }

  getOnlySubjectWiseAttendance(selectedSubject) {
    this.attendanceData = [];
    if (selectedSubject == "All")
      this.attendanceData = [...this.attendanceDataLocal];
    else
      this.attendanceData = this.attendanceDataLocal.filter(
        (data) => data.subjectName == selectedSubject
      );
  }

  getMonthAndStatusWiseAttendance(selectedMonth, selectedStatus) {
    this.attendanceData = [];
    this.statusWiseAttendanceData = [];
    if (selectedStatus == "Both")
      this.statusWiseAttendanceData = [...this.attendanceDataLocal];
    else
      this.statusWiseAttendanceData = this.attendanceDataLocal.filter(
        (data) => data.status == selectedStatus
      );
    if (selectedMonth == "13")
      this.attendanceData = [...this.statusWiseAttendanceData];
    else
      this.attendanceData = this.statusWiseAttendanceData.filter(
        (data) => data.monthOfDate == selectedMonth
      );
  }

  getMonthAndSubjectWiseAttendance(selectedMonth, selectedSubject) {
    this.attendanceData = [];
    this.subjectWiseAttendanceData = [];
    if (selectedSubject == "All")
      this.subjectWiseAttendanceData = [...this.attendanceDataLocal];
    else
      this.subjectWiseAttendanceData = this.attendanceDataLocal.filter(
        (data) => data.subjectName == selectedSubject
      );
    if (selectedMonth == "13")
      this.attendanceData = [...this.subjectWiseAttendanceData];
    else
      this.attendanceData = this.subjectWiseAttendanceData.filter(
        (data) => data.monthOfDate == selectedMonth
      );
  }

  getStatusAndSubjectWiseAttendance(selectedStatus, selectedSubject) {
    this.attendanceData = [];
    this.statusWiseAttendanceData = [];
    if (selectedStatus == "Both")
      this.statusWiseAttendanceData = [...this.attendanceDataLocal];
    else
      this.statusWiseAttendanceData = this.attendanceDataLocal.filter(
        (data) => data.status == selectedStatus
      );
    if (selectedSubject == "All")
      this.attendanceData = [...this.statusWiseAttendanceData];
    else
      this.attendanceData = this.statusWiseAttendanceData.filter(
        (data) => data.subjectName == selectedSubject
      );
  }

  getStatusSubjectMonthWiseAttendance(
    selectedStatus,
    selectedSubject,
    selectedMonth
  ) {
    this.attendanceData = [];
    this.statusWiseAttendanceData = [];
    this.subjectWiseAttendanceData = [];
    if (selectedStatus == "Both")
      this.statusWiseAttendanceData = [...this.attendanceDataLocal];
    else
      this.statusWiseAttendanceData = this.attendanceDataLocal.filter(
        (data) => data.status == selectedStatus
      );
    if (selectedSubject == "All")
      this.subjectWiseAttendanceData = [...this.statusWiseAttendanceData];
    else
      this.subjectWiseAttendanceData = this.statusWiseAttendanceData.filter(
        (data) => data.subjectName == selectedSubject
      );
    if (selectedMonth == "13")
      this.attendanceData = [...this.subjectWiseAttendanceData];
    else
      this.attendanceData = this.subjectWiseAttendanceData.filter(
        (data) => data.monthOfDate == selectedMonth
      );
  }

  getTopFiveMonthAttendance() {
    let presentCount: number = 0;
    let absentCount: number = 0;
    this.attendanceDataLocal.map((data) => {
      console.log("attendance status", data.status);
      if (data.status == "Present") presentCount++;
      else if (data.status == "Absent") absentCount++;
    });
    this.pie = new Chart(this.pieChart1.nativeElement, {
      type: "pie",
      data: {
        labels: ["Present", "Absent"],
        datasets: [
          {
            label: "Attendance Details",
            data: [presentCount, absentCount],
            backgroundColor: ["#2dd36f", "#eb445a"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: true,
        },
      },
    });
  }

  doRefresh(event) {
    if (this.internetService.networkConnected) this.getAttendanceData();
    else {
      this.getMoreStudAttendanceData();
      this.getDistinctSubjects();
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
    }
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
}
