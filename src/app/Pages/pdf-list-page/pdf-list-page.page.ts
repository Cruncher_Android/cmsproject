import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { FileInfo } from "src/app/Models/fileInfo";
import { PreviewAnyFile } from "@ionic-native/preview-any-file/ngx";
import { NavController } from "@ionic/angular";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "app-pdf-list-page",
  templateUrl: "./pdf-list-page.page.html",
  styleUrls: ["./pdf-list-page.page.scss"],
})
export class PdfListPagePage implements OnInit {
  type: string = "";
  subjects: string[] = [];
  selectedSubjects: string[] = [];
  subjectDetails: {
    subjectId: string;
    subjectName: string;
  }[] = [];
  fileInfo: FileInfo[] = [];
  selectedSubject: string = "0";
  branchId: string = "";
  classId: string = "";
  pdfType: string = "";
  pdfTypeUrl: string = "";
  constructor(
    private previewAnyFile: PreviewAnyFile,
    private activatedRoute: ActivatedRoute,
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private navController: NavController,
    private inAppBrowser: InAppBrowser
  ) {}

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.params["type"];
    this.branchId = this.storageService.userDetails.branchId;
    this.classId = this.storageService.userDetails.classId;
    this.pdfType = this.storageService.pdftype;
    this.pdfTypeUrl = this.storageService.pdftypeget;
    this.subjects = this.storageService.userDetails.subjectName.split(",");
    this.selectedSubjects = this.storageService.userDetails.selectedSubjects.split(
      ","
    );
    this.subjectDetails = [];
    for (let i = 0; i < this.subjects.length; i++) {
      this.subjectDetails.push({
        subjectId: this.selectedSubjects[i],
        subjectName: this.subjects[i],
      });
    }
    this.selectedSubject = this.subjectDetails[0].subjectId;
    this.getPdfListSubjectWise(
      this.branchId,
      this.classId,
      this.selectedSubject,
      this.pdfType
    );
  }

  onSelectChange() {
    this.getPdfListSubjectWise(
      this.branchId,
      this.classId,
      this.selectedSubject,
      this.pdfType
    );
  }

  getPdfListSubjectWise(branchId, classId, selectedId, pdfType) {
    this.apiService
      .getPdfListSubjectWise(branchId, classId, selectedId, pdfType)
      .then((data) => {
        this.fileInfo = JSON.parse(data.data).d;
        console.log(this.fileInfo);
      });
  }

  onFileClick(fileName) {
    // this.previewAnyFile
    //   .preview(
    //     `${this.apiService.fileUrl}StudentAppPDF/${this.pdfTypeUrl}/${fileName}`
    //   )
    //   .then(
    //     (data) => console.log("open successfull", data),
    //     (err) => console.log("err", err)
    //   );
    this.inAppBrowser.create(
      `https://docs.google.com/viewer?url=${this.apiService.fileUrl}StudentAppPDF/${this.pdfTypeUrl}/${fileName}&embedded=true`,
      "_self",
      { location: "no" }
    );
  }

  doRefresh(event) {
    this.getPdfListSubjectWise(
      this.branchId,
      this.classId,
      this.selectedSubject,
      this.pdfType
    );
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  onBackClick() {
    this.navController.back();
  }
}
