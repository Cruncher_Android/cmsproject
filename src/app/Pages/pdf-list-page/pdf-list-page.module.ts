import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PdfListPagePageRoutingModule } from './pdf-list-page-routing.module';

import { PdfListPagePage } from './pdf-list-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PdfListPagePageRoutingModule
  ],
  declarations: [PdfListPagePage]
})
export class PdfListPagePageModule {}
