import { Component, OnInit } from "@angular/core";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { SaveTestInfoServiceService } from "./../../Services/save-test-info-service.service";

@Component({
  selector: "app-online-saved-test-page",
  templateUrl: "./online-saved-test-page.page.html",
  styleUrls: ["./online-saved-test-page.page.scss"],
})
export class OnlineSavedTestPagePage implements OnInit {
  testDetails: {
    testId: number;
    testDate: string;
    subjectName: string;
    obtainedMarks: number;
    totalMarks: number;
    testSubmitted: string;
  }[] = [];

  constructor(
    public storageService: StorageServiceService,
    private saveTestInfoService: SaveTestInfoServiceService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.getTest();
  }

  getTest() {
    this.testDetails = [];
    // console.log('in get test page')
    this.saveTestInfoService.getTest().subscribe((data) => {
      this.testDetails = data;
      // console.log('test in page', this.testDetails);
    });
  }

  onTestClick() {
    this.storageService.page = "saved-test";
  }
}
