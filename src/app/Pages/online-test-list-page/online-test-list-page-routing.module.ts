import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OnlineTestListPagePage } from './online-test-list-page.page';

const routes: Routes = [
  {
    path: '',
    component: OnlineTestListPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OnlineTestListPagePageRoutingModule {}
