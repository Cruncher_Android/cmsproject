import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OnlineTestListPagePageRoutingModule } from './online-test-list-page-routing.module';

import { OnlineTestListPagePage } from './online-test-list-page.page';
import { TestDetailsComponentComponent } from './../../Components/test-details-component/test-details-component.component';


@NgModule({
  imports: [
  CommonModule,
    FormsModule,
    IonicModule,
    OnlineTestListPagePageRoutingModule
  ],
  declarations: [OnlineTestListPagePage, TestDetailsComponentComponent]
})
export class OnlineTestListPagePageModule {}
