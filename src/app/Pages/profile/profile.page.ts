import { Component, OnInit } from "@angular/core";
import { UserDetailsService } from "src/app/Services/user-details.service";
import { profileInfo } from "src/app/Models/userInfo";
import { StorageServiceService } from "./../../Services/storage-service.service";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  profileInfo: profileInfo = {
    batchName: this.storageService.userDetails.batchName,
    branchName: this.storageService.userDetails.branchName,
    className: this.storageService.userDetails.className,
    email: this.storageService.userDetails.email,
    parentContact: this.storageService.userDetails.parentContact,
    studentContact: this.storageService.userDetails.studentContact,
    studentName: this.storageService.userDetails.studentName,
    subjectName: this.storageService.userDetails.subjectName,
  };
  constructor(public storageService: StorageServiceService) {}

  ngOnInit() {}
}
