import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OfflineResultPagePage } from './offline-result-page.page';

describe('OfflineResultPagePage', () => {
  let component: OfflineResultPagePage;
  let fixture: ComponentFixture<OfflineResultPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfflineResultPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OfflineResultPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
