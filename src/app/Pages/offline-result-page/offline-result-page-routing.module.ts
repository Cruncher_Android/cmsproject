import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfflineResultPagePage } from './offline-result-page.page';

const routes: Routes = [
  {
    path: '',
    component: OfflineResultPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OfflineResultPagePageRoutingModule {}
