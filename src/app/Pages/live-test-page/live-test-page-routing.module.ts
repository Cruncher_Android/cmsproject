import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiveTestPagePage } from './live-test-page.page';

const routes: Routes = [
  {
    path: '',
    component: LiveTestPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LiveTestPagePageRoutingModule {}
