import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DashboardPagePage } from "./dashboard-page.page";

const routes: Routes = [
  {
    path: "",
    component: DashboardPagePage,
    children: [
      {
        path: "home-page",
        loadChildren: () =>
          import("../home-page/home-page.module").then(
            (m) => m.HomePagePageModule
          ),
      },
      {
        path: "fees-page",
        loadChildren: () =>
          import("../fees-page/fees-page.module").then(
            (m) => m.FeesPagePageModule
          ),
      },
      {
        path: "notifications-page",
        loadChildren: () =>
          import("../notifications-page/notifications-page.module").then(
            (m) => m.NotificationsPagePageModule
          ),
      },
      {
        path: "profile",
        loadChildren: () =>
          import("../profile/profile.module").then((m) => m.ProfilePageModule),
      },
      {
        path: "attendance-page",
        loadChildren: () =>
          import("../attendance-page/attendance-page.module").then(
            (m) => m.AttendancePagePageModule
          ),
      },
      {
        path: "online-saved-test-page",
        loadChildren: () =>
          import(
            "../online-saved-test-page/online-saved-test-page.module"
          ).then((m) => m.OnlineSavedTestPagePageModule),
      },
      {
        path: "offline-result-page",
        loadChildren: () =>
          import("../offline-result-page/offline-result-page.module").then(
            (m) => m.OfflineResultPagePageModule
          ),
      },
      {
        path: "class-time-table-page/:id",
        loadChildren: () =>
          import("../class-time-table-page/class-time-table-page.module").then(
            (m) => m.ClassTimeTablePagePageModule
          ),
      },
      {
        path: "daily-practice-paper-page",
        loadChildren: () =>
          import(
            "../daily-practice-paper-page/daily-practice-paper-page.module"
          ).then((m) => m.DailyPracticePaperPagePageModule),
      },
      {
        path: "video-page",
        loadChildren: () =>
          import("../video-page/video-page.module").then(
            (m) => m.VideoPagePageModule
          ),
      },
      {
        path: "pdf-page",
        loadChildren: () =>
          import("../pdf-page/pdf-page.module").then(
            (m) => m.PdfPagePageModule
          ),
      },
      {
        path: "about-us-page",
        loadChildren: () =>
          import("../about-us-page/about-us-page.module").then(
            (m) => m.AboutUsPagePageModule
          ),
      },
      {
        path: "live-test-page",
        loadChildren: () =>
          import("../live-test-page/live-test-page.module").then(
            (m) => m.LiveTestPagePageModule
          ),
      },
      {
        path: "offline-exam-schedule-page",
        loadChildren: () =>
          import(
            "../offline-exam-schedule-page/offline-exam-schedule-page.module"
          ).then((m) => m.OfflineExamSchedulePagePageModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPagePageRoutingModule {}
