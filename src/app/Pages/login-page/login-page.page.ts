import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ApiServiceService } from "./../../Services/api-service.service";
import { HTTP } from "@ionic-native/http/ngx";
import { Device } from "@ionic-native/device/ngx";
import { Storage } from "@ionic/storage";
import { UserInfo } from "src/app/Models/userInfo";
import { AlertServiceService } from "./../../Services/alert-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { UserDetailsService } from "./../../Services/user-details.service";
import { DatabaseService } from "./../../Services/database.service";
import { InternetServiceService } from "src/app/Services/internet-service.service";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.page.html",
  styleUrls: ["./login-page.page.scss"],
})
export class LoginPagePage implements OnInit {
  loginCredentials: string = "";
  error: string = "";
  userInfo: UserInfo;
  showErrorMessage: boolean = false;
  showValidationMessage: boolean = false;
  deviceId: string = "";

  constructor(
    private router: Router,
    private http: HttpClient,
    private nativeHttp: HTTP,
    private storage: Storage,
    private device: Device,
    private apiService: ApiServiceService,
    private alertService: AlertServiceService,
    private storageService: StorageServiceService,
    private userDetailsService: UserDetailsService,
    private databaseService: DatabaseService,
    private internetService: InternetServiceService
  ) {}

  ngOnInit() {
    this.deviceId = this.device.uuid;
    this.storageService.deviceId = this.deviceId;
  }
  
  onLoginWithOTP() {
    if (this.internetService.networkConnected) {
      this.apiService.login(this.loginCredentials).then((data) => {
        this.userInfo = JSON.parse(data.data).d[0];
        console.log("log in details", this.userInfo);
        this.storageService.userInfo = JSON.parse(data.data).d[0];
        if (this.userInfo.resultCode == "0") {
          this.showErrorMessage = true;
        } else if (
          this.userInfo.DeviceId == "" ||
          this.userInfo.DeviceId == this.deviceId
        ) {
          this.validationForOTP();
        } else if (
          this.userInfo.LoginStatus == "0" ||
          this.userInfo.LoginStatus == "loggedOut"
        ) {
          this.validationForOTP();
        } else {
          this.showValidationMessage = true;
        }
      });
    }
  }

  validationForOTP() {
    this.loginCredentials = "";
    this.showErrorMessage = false;
    this.showValidationMessage = false;
    this.storageService.forgotPassword = false;
    this.router.navigateByUrl("/otp-page");
    this.databaseService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.userDetailsService.insertIntoCMSLogStatus();
      }
    });
  }

  onLoginWithPass() {
    if (this.internetService.networkConnected) {
      this.apiService.login(this.loginCredentials).then(
        (data) => {
          console.log("log in details", JSON.parse(data.data).d[0]);
          this.userInfo = JSON.parse(data.data).d[0];
          this.storageService.userInfo = JSON.parse(data.data).d[0];
          if (this.userInfo.resultCode == "0") {
            this.showErrorMessage = true;
          } else {
            this.loginCredentials = "";
            console.log(this.userInfo);
            if (this.userInfo.AppPassword == "") {
              if (
                this.userInfo.DeviceId == "" ||
                this.userInfo.DeviceId == this.deviceId
              ) {
                this.validationForForgotPassword();
              } else if (
                this.userInfo.LoginStatus == "0" ||
                this.userInfo.LoginStatus == "loggedOut"
              ) {
                this.validationForForgotPassword();
              } else {
                this.showValidationMessage = true;
              }
            } else {
              if (
                this.userInfo.DeviceId == "" ||
                this.userInfo.DeviceId == this.deviceId
              ) {
                this.validationForPassword();
              } else if (
                this.userInfo.LoginStatus == "0" ||
                this.userInfo.LoginStatus == "loggedOut"
              ) {
                this.validationForPassword();
              } else {
                this.showValidationMessage = true;
              }
            }
          }
        },
        (err) => console.log(err)
      );
    } else {
      console.log("in else");
      this.alertService.createAlert(
        "Check your internet connection before proceeding to login"
      );
    }
  }

  validationForPassword() {
    this.router.navigateByUrl("/password-page");
    this.showErrorMessage = false;
    this.showValidationMessage = false;
    this.storageService.firstTimeLogin = false;
    this.databaseService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.userDetailsService.insertIntoCMSLogStatus();
      }
    });
  }

  validationForForgotPassword() {
    this.router.navigateByUrl("/forgot-password-page");
    this.showErrorMessage = false;
    this.showValidationMessage = false;
    this.storageService.firstTimeLogin = true;
    this.databaseService.getDatabaseState().subscribe((ready) => {
      if (ready) {
        this.userDetailsService.insertIntoCMSLogStatus();
      }
    });
  }

  validateUser() {}
}
