import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { CalenderComponentModule } from "./../../Components/calender-component/calender-component.module";
import { ClassTimeTablePagePageRoutingModule } from "./class-time-table-page-routing.module";
import { ClassTimeTablePagePage } from "./class-time-table-page.page";
import { NgCalendarModule } from "ionic2-calendar";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalenderComponentModule,
    NgCalendarModule,
    ClassTimeTablePagePageRoutingModule
  ],
  declarations: [ClassTimeTablePagePage],
})
export class ClassTimeTablePagePageModule {}
