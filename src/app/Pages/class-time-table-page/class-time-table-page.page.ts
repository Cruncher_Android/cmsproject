import { Component, OnInit, ViewChild } from "@angular/core";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { FileInfoTimeTable } from "src/app/Models/fileInfo";
import { TimeTableServiceService } from "./../../Services/time-table-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";
import { ActivatedRoute } from "@angular/router";
import { PreviewAnyFile } from "@ionic-native/preview-any-file/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { CalendarComponent } from "ionic2-calendar";

@Component({
  selector: "app-class-time-table-page",
  templateUrl: "./class-time-table-page.page.html",
  styleUrls: ["./class-time-table-page.page.scss"],
})
export class ClassTimeTablePagePage implements OnInit {
  fileInfo: FileInfoTimeTable[] = [];
  fileInfoDatabase: FileInfoTimeTable[] = [];
  eventSource = [];
  userId: string = "";
  branchId: string = "";
  classId: string = "";
  batchId: string = "";
  category: string = "";
  selectedDate: Date;

  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  viewTitle: string;
  calendar = {
    mode: "month",
    currentDate: new Date(),
  };

  constructor(
    private photoViewer: PhotoViewer,
    private previewAnyFile: PreviewAnyFile,
    private activatedRoute: ActivatedRoute,
    private internetService: InternetServiceService,
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private timeTableService: TimeTableServiceService,
    private toastService: ToastServiceService,
    private inAppBrowser: InAppBrowser
  ) {}

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.category = this.activatedRoute.snapshot.params["id"];
    console.log("category", this.category);
    this.eventSource = [];
    this.userId = this.storageService.userDetails.userId;
    this.branchId = this.storageService.userDetails.branchId;
    this.classId = this.storageService.userDetails.classId;
    this.batchId = this.storageService.userDetails.batchId;
    if (this.internetService.networkConnected) {
      this.getTimeTable(this.userId, this.branchId, this.classId, this.batchId);
    } else {
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
      this.showDatePickerWithClassTTDate();
    }
  }

  getTimeTable(userId, branchId, classId, batchId) {
    this.apiService.getTimeTable(branchId, classId, batchId).then(
      (result) => {
        let data = JSON.parse(result.data);
        let length = data.d.length;
        if (length > 0) {
          this.timeTableService.deleteFromCMSTimeTable().then((_) => {
            this.fileInfo = data.d;
            console.log("file info", this.fileInfo);
            this.fileInfo.map((file) => {
              this.timeTableService.insertIntoCMSTimeTable(
                file.Description,
                file.Date,
                file.FileName,
                file.Category,
                file.AttachmentDescription
              );
            });
            this.showDatePickerWithClassTTDate();
          });
        } else {
          this.timeTableService.deleteFromCMSTimeTable().then((_) => {
            this.showDatePickerWithClassTTDate();
          });
        }
      },
      (err) => {
        this.showDatePickerWithClassTTDate();
      }
    );
  }

  showDatePickerWithClassTTDate() {
    this.eventSource = [];
    this.timeTableService.selectFromCMSTimeTable(this.category).then((data) => {
      this.fileInfoDatabase = data;
      console.log("this.fileInfoDatabase", this.fileInfoDatabase);
      this.fileInfoDatabase.map((fileInfo) => {
        let selectedDate = fileInfo.Date.split(" ")[0];
        let date = Number.parseInt(selectedDate.split("-")[0]);
        let month = Number.parseInt(selectedDate.split("-")[1]);
        let year = Number.parseInt(selectedDate.split("-")[2]);
        let startTime = new Date(year, month - 1, date + 1);
        let endTime = new Date(year, month - 1, date + 1);
        this.eventSource.push({
          title: `${fileInfo.Description} - ${fileInfo.AttachmentDescription}`,
          startTime: startTime,
          endTime: endTime,
          allDay: true,
          desc: fileInfo.FileName,
        });
      });
    });
  }

  async handleEventClicked(event) {
    console.log(event);
    if (this.internetService.networkConnected) {
      if (
        event.desc.split(".")[1] == "jpeg" ||
        event.desc.split(".")[1] == "jpg" ||
        event.desc.split(".")[1] == "gif" ||
        event.desc.split(".")[1] == "png"
      )
        this.photoViewer.show(
          `${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}`
        );
      else
        // this.previewAnyFile.preview(
        //   `${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}`
        // );
        this.inAppBrowser.create(
          `https://docs.google.com/viewer?url=${this.apiService.fileUrl}PDF/StudentTimeTableFile/${event.desc}&embedded=true`,
          "_self",
          { location: "no" }
        );
    } else
      this.toastService.createToast(
        "Check your internet connection to proceed",
        3000
      );
  }

  doRefresh(event) {
    if (this.internetService.networkConnected) {
      this.getTimeTable(this.userId, this.branchId, this.classId, this.batchId);
    } else {
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
      this.showDatePickerWithClassTTDate();
    }
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  next() {
    this.myCal.slideNext();
  }

  back() {
    this.myCal.slidePrev();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }

  // onEventSelected(event) {
  //   // console.log(event)
  //   this.onEventClicked.emit(event);
  // }
}
