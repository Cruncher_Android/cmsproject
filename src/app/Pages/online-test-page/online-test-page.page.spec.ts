import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OnlineTestPagePage } from './online-test-page.page';

describe('OnlineTestPagePage', () => {
  let component: OnlineTestPagePage;
  let fixture: ComponentFixture<OnlineTestPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineTestPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OnlineTestPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
