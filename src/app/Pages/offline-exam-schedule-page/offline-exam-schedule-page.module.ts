import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OfflineExamSchedulePagePageRoutingModule } from './offline-exam-schedule-page-routing.module';

import { OfflineExamSchedulePagePage } from './offline-exam-schedule-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OfflineExamSchedulePagePageRoutingModule
  ],
  declarations: [OfflineExamSchedulePagePage]
})
export class OfflineExamSchedulePagePageModule {}
