import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { StorageServiceService } from "src/app/Services/storage-service.service";
import { OtpServiceService } from "src/app/Services/otp-service.service";
import { UserDetailsService } from "src/app/Services/user-details.service";
import { ApiServiceService } from "src/app/Services/api-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { NavController, AlertController } from "@ionic/angular";
import { AlertServiceService } from "src/app/Services/alert-service.service";

@Component({
  selector: "app-otp-page",
  templateUrl: "./otp-page.page.html",
  styleUrls: ["./otp-page.page.scss"],
})
export class OtpPagePage implements OnInit {
  @ViewChild("otpField1") otpField1;
  showErrorMessage: boolean = false;
  otp1: number;
  otp2: number;
  otp3: number;
  otp4: number;
  securityCode: number;
  showSecurityCode: boolean = false;
  forgotPassword: boolean;

  constructor(
    private navController: NavController,
    private alertController: AlertController,
    private storageService: StorageServiceService,
    private otpService: OtpServiceService,
    private userDetailsService: UserDetailsService,
    private apiService: ApiServiceService,
    private toastService: ToastServiceService,
    private alertService: AlertServiceService
  ) {}

  ngOnInit() {
    this.forgotPassword = this.storageService.forgotPassword;
    if (!this.forgotPassword) {
      if (this.storageService.userInfo.SecurityCode == 0) {
        this.securityCode = Math.floor(1000 + Math.random() * 9000);
        console.log(this.securityCode);
        this.apiService
          .setSecurityCode(
            this.storageService.userInfo.ClientId,
            this.storageService.userInfo.UserId,
            this.securityCode
          )
          .then((_) => {
            // this.createAlert(`Your security code id ${this.securityCode}`);
            this.showSecurityCode = true;
            setTimeout(() => {
              this.otpField1.setFocus();
            }, 500);
          });
      } else {
        this.securityCode = this.storageService.userInfo.SecurityCode;
      }
    } else if (this.forgotPassword) {
      this.securityCode = Math.floor(1000 + Math.random() * 9000);
      console.log("security code", this.securityCode);
      this.toastService
        .createToast("Sending OTP check your mail...", 3000)
        .then((_) => {
          this.apiService
            .sendOTPThroughMail(
              this.storageService.userInfo.Email,
              this.securityCode
            )
            .then(
              (data) => {
                console.log("data", data.data);
                this.toastService
                  .createToast("OTP sent successfully", 3000)
                  .then((_) => {
                    setTimeout(() => {
                      this.otpField1.setFocus();
                    }, 500);
                  });
              },
              (err) => this.toastService.createToast("Failed to send OTP", 3000)
            );
        });
    }

    // console.log(this.number);
    // console.log(this.storageService.userInfo.StudentContact);
    // this.otpService.sendOtp(
    //   this.storageService.userInfo.StudentContact,
    //   this.randomNumber
    // );
  }

  // ionViewDidEnter() {
  //   console.log("view enter", this.otpField1);
  //   // this.otpField1.nativeElement.focus();
  // }

  // ionViewDidLoad() {
  //   console.log("view load", this.otpField1);
  // }

  onContinueClick() {
    let enteredOTP = Number.parseInt(
      `${this.otp1}${this.otp2}${this.otp3}${this.otp4}`
    );
    console.log(enteredOTP);
    if (enteredOTP == this.securityCode) {
      this.apiService
        .createPassword(
          this.storageService.userInfo.UserId,
          this.storageService.password
        )
        .then((data) => {
          console.log(JSON.parse(data.data).d[0]);
          if (JSON.parse(data.data).d[0] == null) {
            this.toastService.createToast(
              "Password changed successfully",
              3000
            );
            this.userDetailsService.getStudentAccess(
              this.storageService.userInfo.UserId
            );
            // this.router.navigateByUrl("/home-page");
          }
        });
    } else this.showErrorMessage = true;
  }

  onContinueClick1() {
    let enteredOTP = Number.parseInt(
      `${this.otp1}${this.otp2}${this.otp3}${this.otp4}`
    );
    console.log(enteredOTP);
    if (enteredOTP == this.securityCode) {
      this.userDetailsService.getStudentAccess(
        this.storageService.userInfo.UserId
      );
    } else this.showErrorMessage = true;
  }

  move(next, event) {
    console.log("key", event.key);
    if (event.key != "Backspace") next.setFocus();
    // let length = from.length;
    // if(length == 1) {
    //   to.setFocus();
    // }
  }

  async createAlert(message) {
    const alert = await this.alertController.create({
      animated: true,
      backdropDismiss: false,
      subHeader: message,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
        },
      ],
      cssClass: "alert-title",
    });
    await alert.present();
    alert.onDidDismiss().then((_) => {
      setTimeout(() => {
        this.otpField1.setFocus();
      }, 500);
    });
  }

  onBackClick() {
    this.navController.back();
  }
}
