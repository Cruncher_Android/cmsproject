import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OtpPagePage } from './otp-page.page';

describe('OtpPagePage', () => {
  let component: OtpPagePage;
  let fixture: ComponentFixture<OtpPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtpPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OtpPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
