import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExamTimeTablePagePageRoutingModule } from './exam-time-table-page-routing.module';

import { ExamTimeTablePagePage } from './exam-time-table-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExamTimeTablePagePageRoutingModule
  ],
  declarations: [ExamTimeTablePagePage]
})
export class ExamTimeTablePagePageModule {}
