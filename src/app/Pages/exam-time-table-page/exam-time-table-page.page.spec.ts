import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExamTimeTablePagePage } from './exam-time-table-page.page';

describe('ExamTimeTablePagePage', () => {
  let component: ExamTimeTablePagePage;
  let fixture: ComponentFixture<ExamTimeTablePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExamTimeTablePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExamTimeTablePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
