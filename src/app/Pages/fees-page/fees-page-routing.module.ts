import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeesPagePage } from './fees-page.page';

const routes: Routes = [
  {
    path: '',
    component: FeesPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeesPagePageRoutingModule {}
