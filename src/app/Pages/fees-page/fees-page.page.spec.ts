import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FeesPagePage } from './fees-page.page';

describe('FeesPagePage', () => {
  let component: FeesPagePage;
  let fixture: ComponentFixture<FeesPagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeesPagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeesPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
