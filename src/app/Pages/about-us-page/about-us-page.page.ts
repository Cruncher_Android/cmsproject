import { Component, OnInit } from "@angular/core";
import { ApiServiceService } from "./../../Services/api-service.service";
import { AboutUs } from "src/app/Models/aboutUs";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { StorageServiceService } from 'src/app/Services/storage-service.service';

@Component({
  selector: "app-about-us-page",
  templateUrl: "./about-us-page.page.html",
  styleUrls: ["./about-us-page.page.scss"],
})
export class AboutUsPagePage implements OnInit {
  details: AboutUs = {
    ClientId: 0,
    aboutus: "",
    address: "",
    email_id: "",
    name: "",
    websiteURL: "",
  };
  constructor(
    private inAppBrowser: InAppBrowser,
    private apiService: ApiServiceService,
    public storageService: StorageServiceService    
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.apiService.getAboutUsDetails().then((data) => {
      // console.log(JSON.parse(data.data));
      this.details = JSON.parse(data.data).d[0];
      console.log("aboutus", this.details);
      document.getElementById("about-us").innerHTML = this.details.aboutus;
    });
  }

  onTitleClick() {
    if (this.details.websiteURL != null || this.details.websiteURL != "")
      this.inAppBrowser.create(this.details.websiteURL, "_self", {
        location: "no",
      });
  }
}
