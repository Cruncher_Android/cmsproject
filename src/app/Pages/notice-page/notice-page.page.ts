import { Component, OnInit } from "@angular/core";
import { NotificationServiceService } from "./../../Services/notification-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { Notice, LastNotification } from "src/app/Models/notificationInfo";
import { ApiServiceService } from "./../../Services/api-service.service";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-notice-page",
  templateUrl: "./notice-page.page.html",
  styleUrls: ["./notice-page.page.scss"],
})
export class NoticePagePage implements OnInit {
  sid: string = "";
  noticeDetails: Notice[] = [];
  lastNotification: LastNotification;

  constructor() {}

  ngOnInit() {}
}
