import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoticePagePage } from './notice-page.page';

describe('NoticePagePage', () => {
  let component: NoticePagePage;
  let fixture: ComponentFixture<NoticePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoticePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
