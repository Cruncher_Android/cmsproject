import { Component, OnInit, ViewChild } from "@angular/core";
import { InternetServiceService } from "./../../Services/internet-service.service";
import { StorageServiceService } from "./../../Services/storage-service.service";
import { ApiServiceService } from "./../../Services/api-service.service";
import { FileInfoDPP } from "src/app/Models/fileInfo";
import { ToastServiceService } from "./../../Services/toast-service.service";
import { PhotoViewer } from "@ionic-native/photo-viewer/ngx";
import { DailyPracticePaperServiceService } from "./../../Services/daily-practice-paper-service.service";
import { PreviewAnyFile } from "@ionic-native/preview-any-file/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { CalendarComponent } from "ionic2-calendar";

@Component({
  selector: "app-daily-practice-paper-page",
  templateUrl: "./daily-practice-paper-page.page.html",
  styleUrls: ["./daily-practice-paper-page.page.scss"],
})
export class DailyPracticePaperPagePage implements OnInit {
  eventSource = [];

  fileInfo: FileInfoDPP[] = [];
  fileInfoDatabase: FileInfoDPP[] = [];

  userId: string = "";
  branchId: string = "";
  classId: string = "";
  batchId: string = "";
  selectedDate: Date;

  @ViewChild(CalendarComponent) myCal: CalendarComponent;

  viewTitle: string;
  calendar = {
    mode: "month",
    currentDate: new Date(),
  };

  constructor(
    private photoViewer: PhotoViewer,
    private previewAnyFile: PreviewAnyFile,
    private internetService: InternetServiceService,
    public storageService: StorageServiceService,
    private apiService: ApiServiceService,
    private dailyPracticePaperService: DailyPracticePaperServiceService,
    private toastService: ToastServiceService,
    private inAppBrowser: InAppBrowser

  ) {}

  ngOnInit() {
    
  }

  ionViewDidEnter(){
    this.userId = this.storageService.userDetails.userId;
    this.branchId = this.storageService.userDetails.branchId;
    this.classId = this.storageService.userDetails.classId;
    this.batchId = this.storageService.userDetails.batchId;
    if (this.internetService.networkConnected) {
      this.getDailyPracticePaper(
        this.userId,
        this.branchId,
        this.classId,
        this.batchId
      );
    } else {
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
      this.showDatePickerWithDppDate();
    }
  }

  getDailyPracticePaper(userId, branchId, classId, batchId) {
    this.apiService.getDailyPracticePaper(branchId, classId, batchId).then(
      (result) => {
        let data = JSON.parse(result.data);
        let length = data.d.length;
        if (length > 0) {
          this.dailyPracticePaperService
            .deleteFromCMSDailyPracticePaper()
            .then((_) => {
              this.fileInfo = data.d;
              console.log(this.fileInfo);
              this.fileInfo.map((file) => {
                this.dailyPracticePaperService.insertIntoCMSDailyPracticePaper(
                  file.Description,
                  file.Date,
                  file.FileName,
                  file.AttachmentDescription
                );
              });
              this.showDatePickerWithDppDate();
            });
        } else {
          this.dailyPracticePaperService
            .deleteFromCMSDailyPracticePaper()
            .then((_) => {
              this.showDatePickerWithDppDate();
            });
        }
      },
      (err) => {
        this.showDatePickerWithDppDate();
      }
    );
  }

  showDatePickerWithDppDate() {
    this.eventSource = [];
    this.dailyPracticePaperService
      .selectFromCMSDailyPracticePapere()
      .then((data) => {
        this.fileInfoDatabase = data;
        // console.log("this.fileInfoDatabase", this.fileInfoDatabase);
        this.fileInfoDatabase.map((fileInfo) => {
          let selectedDate = fileInfo.Date.split(" ")[0];
          let date = Number.parseInt(selectedDate.split("-")[0]);
          let month = Number.parseInt(selectedDate.split("-")[1]);
          let year = Number.parseInt(selectedDate.split("-")[2]);
          let startTime = new Date(year, month - 1, date + 1);
          let endTime = new Date(year, month - 1, date + 1);
          this.eventSource.push({
            title: `${fileInfo.Description} - ${fileInfo.AttachmentDescription}`,
            startTime: startTime,
            endTime: endTime,
            allDay: true,
            desc: fileInfo.FileName,
          });
        });
      });
  }

  async handleEventClicked(event) {
    console.log(event);
    if (this.internetService.networkConnected) {
      if (
        event.desc.split(".")[1] == "jpeg" ||
        event.desc.split(".")[1] == "jpg" ||
        event.desc.split(".")[1] == "gif" ||
        event.desc.split(".")[1] == "png"
      )
        this.photoViewer.show(
          `${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}`
        );
      else
        // this.previewAnyFile.preview(
        //   `${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}`
        // );
        this.inAppBrowser.create(
          `https://docs.google.com/viewer?url=${this.apiService.fileUrl}PDF/DailyPracticePaperFile/${event.desc}&embedded=true`,
          "_self",
          { location: "no" }
        );
    } else
      this.toastService.createToast(
        "Check your internet connection to proceed",
        3000
      );
  }

  doRefresh(event) {
    if (this.internetService.networkConnected) {
      this.getDailyPracticePaper(
        this.userId,
        this.branchId,
        this.classId,
        this.batchId
      );
    } else {
      this.toastService.createToast(
        "Check internet connection to update details",
        3000
      );
      this.showDatePickerWithDppDate();
    }
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  next() {
    this.myCal.slideNext();
  }

  back() {
    this.myCal.slidePrev();
  }

  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    this.viewTitle = title;
  }
}
