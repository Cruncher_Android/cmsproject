import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DailyPracticePaperPagePage } from './daily-practice-paper-page.page';

const routes: Routes = [
  {
    path: '',
    component: DailyPracticePaperPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DailyPracticePaperPagePageRoutingModule {}
