import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { DailyPracticePaperPagePageRoutingModule } from "./daily-practice-paper-page-routing.module";
import { DailyPracticePaperPagePage } from "./daily-practice-paper-page.page";
import { CalenderComponentModule } from 'src/app/Components/calender-component/calender-component.module';
import { NgCalendarModule } from "ionic2-calendar";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DailyPracticePaperPagePageRoutingModule,
    NgCalendarModule,
    CalenderComponentModule
  ],
  declarations: [DailyPracticePaperPagePage],
})
export class DailyPracticePaperPagePageModule {}
