export interface attendanceDetails {
  date: string;
  status: string;
  subjectName: string;
  batchName: string;
  inTime: string;
  outTime: string;
  monthOfDate: string;
}
