export interface AboutUs {
  ClientId: number;
  name: string;
  aboutus: string;
  address: string;
  email_id: string;
  websiteURL: string;
}
