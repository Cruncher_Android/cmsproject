export interface TestDetails {
    title: string;
    testDate: string;
    EndDate: string;
    startTime: string;
    endTime: string;
    duration: string;
    testPaperId: string;
    sid: string;
    status: string;
    arrengeTestId: string;
    CetCorrect: string;
    CetInCorrect: string;
    NeetCorrect: string;
    NeetInCorrect: string;
    JeeCorrect: string;
    JeeInCorrect: string;
    JeeNewCorrect: string;
    JeeNewInCorrect: string;
    TestType: string;
}